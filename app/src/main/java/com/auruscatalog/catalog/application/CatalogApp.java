package com.auruscatalog.catalog.application;

import android.app.Application;

import com.auruscatalog.catalog.database.DataBase;
import com.auruscatalog.catalog.model.Basket;
import com.auruscatalog.catalog.model.DataController;
import com.auruscatalog.catalog.utility.Utility;
import com.paypal.android.sdk.payments.PayPalConfiguration;

import java.io.Serializable;

public class CatalogApp extends Application implements Serializable
{
	private static CatalogApp instanse;

    private static DataBase dataBase;

	private  DataController mDataController;

	private  Basket mBasket;

    private static PayPalConfiguration paypal_config;
	/**
	 * Тут идут методы класса.
	 */
	@Override
	public void onCreate()
	{
		super.onCreate();

        dataBase = new DataBase(this);
		mDataController = DataController.newInstanse(dataBase);

		mBasket = Basket.getInstanse();
        
        paypal_config = new PayPalConfiguration();
        paypal_config.environment(PayPalConfiguration.ENVIRONMENT_SANDBOX);
        paypal_config.clientId("AaOSLhBTcuHeOdiftDGZgylUaWTuZVv3XZkXnD6z6YdxoYfciEjx43UCZKYG");

		instanse = this;
	}

	public Basket getBasket()
	{
		return mBasket;
	}

	public DataController getDataController()
	{
		return DataController.newInstanse(dataBase);
	}

	public static synchronized CatalogApp getInstanse()
	{
		if(instanse == null)
		{
			instanse = new CatalogApp();
		}
		return instanse;
	}

	public void onCloseDataBase()
	{
		if(dataBase != null)
        {
            dataBase.onClose();
            dataBase = null;
            mDataController.onCloseDB();
        }
	}

	public void onStartDataBase()
	{
		if(dataBase == null) {
            dataBase = new DataBase(this);
            mDataController.onAttachDB(dataBase);
        }
	}
	
	/**
	 * Возвращает конфигурационный объект PayPal
	 */
	public PayPalConfiguration getPPConfig() {
		return paypal_config;
	}
}
