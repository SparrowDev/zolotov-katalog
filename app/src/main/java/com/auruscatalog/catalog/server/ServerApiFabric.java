package com.auruscatalog.catalog.server;

import com.paypal.android.sdk.T;

import retrofit.RestAdapter;

/**
 * Created by sparrow on 21.10.15.
 */
public class ServerApiFabric {

    public static <T> T createRetrofitService(final Class<T> clazz, final String endPoint)
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endPoint)
                .build();

        T service = restAdapter.create(clazz);

        return service;
    }
}
