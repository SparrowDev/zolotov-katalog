package com.auruscatalog.catalog.server;

import com.auruscatalog.catalog.model.Complekt;
import com.auruscatalog.catalog.model.Dictonary;
import com.auruscatalog.catalog.model.Group;
import com.auruscatalog.catalog.model.Image;
import com.auruscatalog.catalog.model.News;
import com.auruscatalog.catalog.model.Product;
import com.auruscatalog.catalog.model.Size;
import com.auruscatalog.catalog.model.Slide;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by sparrow on 21.10.15.
 */
public interface ServerApi {

    String END_POINT = "http://www.zolotovnews.brandhome24.ru/index.php/api";

    @FormUrlEncoded
    @POST("/checkSlide")
    Observable<Slide[]> getSlide(@Field("_revision") String revision);

    @FormUrlEncoded
    @POST("/checkImage")
    Observable<Image[]> getImage(@Field("_revision") String revision);

    @FormUrlEncoded
    @POST("/checkProduct")
    Observable<Product[]> getProduct(@Field("_revision") String revision);

    @FormUrlEncoded
    @POST("/checkProductParametr")
    Observable<Size[]> getParametr(@Field("_revision") String revision);

    @FormUrlEncoded
    @POST("/checkGroup")
    Observable<Group[]> getGroup(@Field("_revision") String revision);

    @FormUrlEncoded
    @POST("/checkComplekt")
    Observable<Complekt[]> getComplekt(@Field("_revision") String revision);

    @FormUrlEncoded
    @POST("/checkParamList")
    Observable<Dictonary[]> getParamList(@Field("_id") String _id, @Field("_revision") String _revision);

    @FormUrlEncoded
    @POST("/checkNews")
    Observable<News[]> getNews(@Field("_id") String _id);


}
