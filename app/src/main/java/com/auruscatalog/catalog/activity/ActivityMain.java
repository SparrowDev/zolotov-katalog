package com.auruscatalog.catalog.activity;

import android.app.Fragment;
import android.app.IntentService;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.fragments.DialogDrop;
import com.auruscatalog.catalog.fragments.ExportDialog;
import com.auruscatalog.catalog.fragments.Fragment_Card;
import com.auruscatalog.catalog.fragments.Fragment_Cart;
import com.auruscatalog.catalog.fragments.Fragment_Catalog;
import com.auruscatalog.catalog.fragments.Fragment_Filter;
import com.auruscatalog.catalog.fragments.Fragment_ImageView;
import com.auruscatalog.catalog.fragments.Fragment_Info;
import com.auruscatalog.catalog.fragments.Fragment_Main;
import com.auruscatalog.catalog.fragments.Fragment_News;
import com.auruscatalog.catalog.fragments.Fragment_Order;
import com.auruscatalog.catalog.fragments.StartScreen;
import com.auruscatalog.catalog.model.Product;
import com.auruscatalog.catalog.utility.FragmentManagerApp;

/**
 * Created by sparrow on 13.10.15.
 */
public class ActivityMain extends AppCompatActivity {

    Toolbar mToolbar;

    FragmentManagerApp mFragmentManagerApp;

    public Menu mMenu;
    protected CatalogApp app;
    protected ActionBar actionBar;

    boolean mFirst = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        actionBar = getSupportActionBar();


        app = (CatalogApp)getApplication();

        mFragmentManagerApp =
                savedInstanceState != null && savedInstanceState.getSerializable("stack") != null ?
                (FragmentManagerApp)savedInstanceState.getSerializable("stack") :
                new FragmentManagerApp(getFragmentManager());

        mFirst = !(savedInstanceState != null && savedInstanceState.containsKey("first")) || savedInstanceState.getBoolean("first");

        setTitle(
                savedInstanceState != null && savedInstanceState.containsKey("title") ?
                        savedInstanceState.getString("title") :
                        getResources().getString(R.string.app_name));


        if(mFirst)
        {
            mFragmentManagerApp.addFragment(StartScreen.newInstanse(), FragmentManagerApp.START);
            mFirst = false;
        }
        else if(!mFragmentManagerApp.isNotVoid())
            mFragmentManagerApp.addFragment(Fragment_Main.getNewInstanse(), FragmentManagerApp.MAIN);

        if(mFragmentManagerApp.getCountStack() > 1)
            setBackBar();
        else
            setRootBar();
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);

        Toast.makeText(getApplicationContext(), Integer.toString(metricsB.widthPixels), Toast.LENGTH_LONG).show();
    }

    /**
     * Вызывает запуск главного Fragment'а приложения
     */
    public void onStartMain()
    {
        mFragmentManagerApp.popFragment();
        mFragmentManagerApp.addFragment(Fragment_Main.getNewInstanse(), FragmentManagerApp.MAIN);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                onCreateMenu(mMenu);
            }
        });
    }

    /**
     * Создание меню
     */
    public boolean onCreateOptionsMenu(Menu menu)
    {
        mMenu = menu;
        if(!mFragmentManagerApp.isTopFromTag(FragmentManagerApp.START))
            onCreateMenu(mMenu);
        return true;
    }

    /**
     * Устанавливает AppBar корневм , без активной кнопки домой и с укороченным меню
     */
    protected void setRootBar()
    {
        if (actionBar == null)
            actionBar = getSupportActionBar();

        if(actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        setTitle("Каталог");
        if(mMenu != null)
            onCreateMenu(mMenu);
    }

    /**
     * Устанавливает AppBar с кнопкой домой и расширенным меню
     */
    public void setBackBar() {
        if (actionBar == null)
            actionBar = getSupportActionBar();

        if(actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(false);
        }
        if(mMenu != null)
            onCreateMenu(mMenu);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            if(mFragmentManagerApp.isTopFromTag(FragmentManagerApp.CATALOG))
            {
                Fragment_Catalog catalog = (Fragment_Catalog)mFragmentManagerApp.getTop();
                if(!catalog.onBack())
                {
                    onBackPressed();
                }
            }
            else
                onBackPressed();
        }
        return true;
    }

    private void setOnFragment(Fragment fragment, String tag)
    {
        mFragmentManagerApp.addFragment(fragment, tag);
        if(mFragmentManagerApp.getCountStack() > 1)
            setBackBar();
        else
            setRootBar();
        setTitle("Фильтр");
    }

    public boolean onCreateMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(
                mFragmentManagerApp.getCountStack() > 1 ?
                        R.menu.menu_catalog :
                        R.menu.menu_card, menu);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean("first", mFirst);
        outState.putString("title", getTitle().toString());
    }

    @Override
    public void onBackPressed() {
        mFragmentManagerApp.popFragment();
        if(mFragmentManagerApp.getCountStack() > 1)
            setBackBar();
        else
            setRootBar();

        if(!mFragmentManagerApp.isNotVoid())
            System.exit(0);
    }

    /**
     * Показвает диалог создания отчета
     */
    public void onShowExportDialog()
    {
        setOnFragment(ExportDialog.getInstanse(), FragmentManagerApp.EXPORT);
        setTitle("Экспорт");
    }

    public void onCompanyClick(View view)
    {
        setOnFragment(Fragment_Info.newInstanse(Fragment_Info.TypeInfo.About), FragmentManagerApp.ABOUT);
        setTitle("О компании");
    }

    public void onContactClick(View view)
    {
        setOnFragment(Fragment_Info.newInstanse(Fragment_Info.TypeInfo.Contac), FragmentManagerApp.ABOUT);
        setTitle("Контакты");
    }

    public void onNewsClick(View view)
    {
        setOnFragment(Fragment_News.newInstance(app), FragmentManagerApp.NEWS);
        setTitle("Новости");
    }

    public void onPartnerClick(View view)
    {
        setOnFragment(Fragment_Info.newInstanse(Fragment_Info.TypeInfo.Parther), FragmentManagerApp.ABOUT);
        setTitle("Сотрудничество");
    }

    public void onCartClick(View view)
    {
        setOnFragment(Fragment_Cart.newInstanse(), FragmentManagerApp.CART);
        setTitle("Корзина");
    }

    public void onCatalogClick(View view)
    {
        setOnFragment(Fragment_Catalog.newInstance(CatalogApp.getInstanse(), false), FragmentManagerApp.CATALOG);
        setTitle("Каталог");
    }

    public void onFilterClick(View view)
    {
        onShowFilter();
    }

    public void onComplektClick(View view)
    {

    }

    public void onNewClick(View view)
    {

    }


    /**
     * Обработчик нажатия кнопки Фильтр.
     */
    public boolean onToFilter(MenuItem item)
    {
        onShowFilter();
        return true;
    }

    public void onShowFilter()
    {
        setOnFragment(Fragment_Filter.getInstanse(), FragmentManagerApp.FILTER);
    }

    public void onCancelFilter()
    {
        onBackPressed();
    }

    public void onAcceptFilter()
    {

    }

    public void onZoom(String image)
    {

    }

    public void onShowProductCard(Product product)
    {
        setOnFragment(Fragment_Card.newInstanse(CatalogApp.getInstanse(), product), FragmentManagerApp.CARD);
        setTitle("Товар");
    }

    /**
     * Обработчик нажатия кнопки Корзина.
     */
    public boolean onToCart(MenuItem item)
    {
        setOnFragment(Fragment_Cart.newInstanse(), FragmentManagerApp.CART);
        setTitle("Корзина");
        return true;
    }

    public void onShowImageViewCard(String imageUrl)
    {
        setOnFragment(Fragment_ImageView.newInstanse(imageUrl), FragmentManagerApp.IMAGEVIEW);
    }

    public void onShowDialogDropCartItem(DialogDrop dialogDrop)
    {
        dialogDrop.show(getFragmentManager(), "dialog");
    }

    @Override
    protected void onResume() {
        super.onResume();
        app.onStartDataBase();
    }

    @Override
    protected void onStop() {
        super.onStop();
        app.onCloseDataBase();
    }

    /**
     * Запускает Fragment оформления заказа
     */
    public void onToOrder()
    {
        setOnFragment(Fragment_Order.newInstanse(), FragmentManagerApp.ORDER);
        setTitle("Оформление заказа");
    }
}
