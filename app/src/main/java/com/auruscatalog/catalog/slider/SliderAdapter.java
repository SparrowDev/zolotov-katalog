package com.auruscatalog.catalog.slider;

import java.util.ArrayList;
import java.util.Map;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.fragments.Fragment_Main;
import com.squareup.picasso.Picasso;

public class SliderAdapter extends PagerAdapter
{
	private JazzyViewPager mJazzy;
	private ArrayList<Map<String, Object>> data;
    private Context context;
	private Picasso mPicasso;

    private CatalogApp app;


    public SliderAdapter(Context context, JazzyViewPager mJazzy, ArrayList<Map<String, Object>> data)
    {
        super();
        this.context = context;
        this.mJazzy = mJazzy;
        this.data = data;


		mPicasso = Picasso.with(context);
        app = CatalogApp.getInstanse();
    }

	@Override
	public Object instantiateItem(final ViewGroup container, final int position)
	{
		final ImageView imageView = new ImageView(context);

        String image_url = "";
        for (int i = 0; i < app.getDataController().getImages().length; i++) {
            if(app.getDataController().getImages()[i].getId() == (int)data.get(position).get("image")) {
				image_url = app.getDataController().getImages()[i].getImageUrl();
				break;
			}
        }

		if(image_url != null && !image_url.equals("") && !image_url.equals(" "))
		{
			mPicasso.load(image_url)
					.into(imageView);
		}


		//imageView.setBackgroundColor(Color.BLACK);
		/*imageView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (data.get(position).get("type") == TypeData.GROUP) {
					//Переходим в группу
					Intent intent = new Intent(context, ActivityCatalog.class);
					intent.putExtra("id", (Integer) data.get(position).get("value"));
					context.startActivity(intent);

				} else if (data.get(position).get("type") == TypeData.PRODUCT) {
					//Переходим в карточку изделия
					Intent intent = new Intent(context, ActivityCard.class);
					intent.putExtra("product_id", (Integer) data.get(position).get("value"));
					context.startActivity(intent);
				}
			}
		});*/

		container.addView(imageView, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		mJazzy.setObjectForPosition(imageView, position);
		return imageView;
	}
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object obj)
	{
		container.removeView(mJazzy.findViewFromObject(position));
	}
	
	@Override
	public int getCount()
	{
		return data.size();
	}

	@Override
	public boolean isViewFromObject(View view, Object obj)
	{
		if (view instanceof OutlineContainer)
		{
			return ((OutlineContainer) view).getChildAt(0) == obj;
		}
		else
		{
			return view == obj;
		}
	}		
}