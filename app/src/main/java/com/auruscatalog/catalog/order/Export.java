package com.auruscatalog.catalog.order;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import android.text.format.DateFormat;

import com.auruscatalog.catalog.utility.Utility;

import jxl.CellFeatures;
import jxl.CellType;
import jxl.LabelCell;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Export
{
	private static WritableCellFormat timesBoldUnderline;
	private static WritableCellFormat times;

	/*public static void toXLS(ArrayList<BasketPosition> arrayList, String path, String email, String name, String phone, String note)
	{
		File file = new File(path);
		//file.delete();
		
		WorkbookSettings wb_settings = new WorkbookSettings();
		wb_settings.setLocale(new Locale("ru", "RU"));
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(file, wb_settings);
			workbook.createSheet("Заказ", 0);
			WritableSheet sheet = workbook.getSheet(0);
			sheet.setColumnView(0, 8);
			sheet.setColumnView(1, 19);
			sheet.setColumnView(2, 19);
			sheet.setColumnView(3, 7);
			sheet.setColumnView(4, 12);
			sheet.setColumnView(5, 8);
			sheet.setColumnView(6, 12);
			
			WritableCellFormat f_center = new WritableCellFormat();
			f_center.setAlignment(Alignment.CENTRE);
			
			Label org_name = new Label(0, 0, "ООО \"Золотов\"");
			Label org_email = new Label(4, 0, "zolotov@zolotov.ru");
			Label org_contact = new Label(0, 1, "Контактный телефон: 8 (495) 785-66-55");
			Label org_order = new Label(0, 2, "Заказ", f_center);
			Label org_client = new Label(0, 3, "Заказчик: " + name);
			Label org_date = new Label(0, 4, "Дата заказа: " + Utility.getDateString());
			Label org_client_email = new Label(0, 5, "Электронный адрес: " + email);
			Label org_client_phone = new Label(0, 6, "Телефон: " + phone);
			Label org_note = new Label(0, 7, "Комментарий: " + note);
			
			sheet.mergeCells(0, 0, 3, 0);
			sheet.mergeCells(4, 0, 6, 0);
			sheet.mergeCells(0, 1, 6, 1);
			sheet.mergeCells(0, 2, 6, 2);
			sheet.mergeCells(0, 3, 6, 3);
			sheet.mergeCells(0, 4, 6, 4);
			sheet.mergeCells(0, 5, 6, 5);
			sheet.mergeCells(0, 6, 6, 6);
			sheet.mergeCells(0, 7, 6, 7);
			
			sheet.addCell(org_name);
			sheet.addCell(org_email);
			sheet.addCell(org_contact);
			sheet.addCell(org_order);
			sheet.addCell(org_client);
			sheet.addCell(org_date);
			sheet.addCell(org_client_email);
			sheet.addCell(org_client_phone);
			sheet.addCell(org_note);
			
			WritableCellFormat f_table_all = new WritableCellFormat();
			f_table_all.setAlignment(Alignment.CENTRE);
			f_table_all.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
			
			Label t_num = new Label(0, 8, "№", f_table_all);
			Label t_article = new Label(1, 8, "Артикул", f_table_all);
			Label t_count = new Label(2, 8, "Количество шт.", f_table_all);
			Label t_weight = new Label(3, 8, "Вес", f_table_all);
			Label t_gross_weight = new Label(4, 8, "Общий вес", f_table_all);
			Label t_price = new Label(5, 8, "Цена", f_table_all);
			Label t_gross_price = new Label(6, 8, "Сумма", f_table_all);
			
			sheet.addCell(t_num);
			sheet.addCell(t_article);
			sheet.addCell(t_count);
			sheet.addCell(t_weight);
			sheet.addCell(t_gross_weight);
			sheet.addCell(t_price);
			sheet.addCell(t_gross_price);
			int i;
			int all_count = 0;
			Double all_weight = 0.0;
			Double all_price = 0.0;
			for(i = 0; i < arrayList.size(); i++)
			{
				all_count += arrayList.get(i).getCount();
				all_weight += arrayList.get(i).getCount() * arrayList.get(i).getProduct().getWeight();
				all_price += arrayList.get(i).getPrice();
				
				Label v_num = new Label(0, 9 + i, Integer.toString(i + 1), f_table_all);
				Label v_article = new Label(1, 9 + i, arrayList.get(i).getProduct().getArticul(), f_table_all);
				Number v_count = new Number(2, 9 + i, arrayList.get(i).getCount(), f_table_all);
				Number v_weight = new Number(3, 9 + i, arrayList.get(i).getProduct().getWeight(), f_table_all);
				Formula v_gross_weight = new Formula(4, 9 + i, "C"+(10 + i)+"*D"+(10 + i), f_table_all);
				Number v_price = new Number(5, 9 + i, arrayList.get(i).getProduct().getPrice(), f_table_all);
				Formula v_gross_price = new Formula(6, 9 + i, "C"+(10 + i)+"*F"+(10 + i), f_table_all);
				
				sheet.addCell(v_num);
				sheet.addCell(v_article);
				sheet.addCell(v_count);
				sheet.addCell(v_weight);
				sheet.addCell(v_gross_weight);
				sheet.addCell(v_price);
				sheet.addCell(v_gross_price);
			}
			
			Label itog = new Label(0, 9 + i, "Итого: ");
			Number g_count = new Number(2, 9 + i, all_count);
			Number g_weight = new Number(4, 9 + i,all_weight);
			Number g_price = new Number(6, 9 + i, all_price);
			
			sheet.addCell(itog);
			sheet.addCell(g_count);
			sheet.addCell(g_weight);
			sheet.addCell(g_price);
			
			workbook.write();
			workbook.close();
		}
		catch (Exception e)
		{
			
		}
	}*/
}