package com.auruscatalog.catalog.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sparrow on 26.10.15.
 */
public class Fragment_Filter extends DialogFragment {

    public class SpinnerAdapter extends ArrayAdapter
    {
        LayoutInflater mInflater;

        String mPrompt;

        public SpinnerAdapter(Context context, int resource, Object[] objects, String prompt) {
            super(context, resource, objects);

            mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mPrompt = prompt;
        }


        @Override
        public String getItem(int position) {
            String result = "";

            if(position == 0)
                result = mPrompt;
            else
                result = super.getItem(position - 1).toString();

            return result;
        }

        @Override
        public int getCount() {
            return super.getCount() + 1;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = mInflater.inflate(R.layout.spinner_item, parent, false);

            ((TextView)convertView.findViewById(android.R.id.text1)).setText(getItem(position));

            return convertView;
        }
    }

    public static Fragment_Filter getInstanse()
    {
        return new Fragment_Filter();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View filter = inflater.inflate(R.layout.fragment_filter, container, false);

        ((Button)filter.findViewById(R.id.btn_accept)).setOnClickListener(acceptClick);
        ((Button)filter.findViewById(R.id.btn_cancel)).setOnClickListener(cancelClick);


        Spinner spinnerWho = (Spinner)filter.findViewById(R.id.spinnerWho);
        Spinner spinnerColor = (Spinner)filter.findViewById(R.id.spinnerColor);
        Spinner spinnerTehn = (Spinner)filter.findViewById(R.id.spinnerTehn);
        Spinner spinnerStone = (Spinner)filter.findViewById(R.id.spinnerStone);

        String[] who = new String[] {"Для мужчин" , "Для женщин"};
        String[] color = new String[] {"Белое золото", "Красное золото", "Черное серебро"};
        String[] stone = new String[] { "Фианит", "Эмаль" };
        String[] teh = new String[] { "Алмазная грань", "Эмаль" };

        spinnerWho.setAdapter(new SpinnerAdapter(getActivity().getApplication(), R.layout.spinner_item, who, spinnerWho.getPrompt().toString()));
        spinnerColor.setAdapter(new SpinnerAdapter(getActivity().getApplication(), R.layout.spinner_item, color, spinnerColor.getPrompt().toString()));
        spinnerTehn.setAdapter(new SpinnerAdapter(getActivity().getApplication(), R.layout.spinner_item, teh, spinnerTehn.getPrompt().toString()));
        spinnerStone.setAdapter(new SpinnerAdapter(getActivity().getApplication(), R.layout.spinner_item, stone, spinnerStone.getPrompt().toString()));

        return filter;
    }

    private View.OnClickListener cancelClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((ActivityMain)getActivity()).onCancelFilter();
        }
    };

    private View.OnClickListener acceptClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((ActivityMain)getActivity()).onAcceptFilter();
        }
    };
}
