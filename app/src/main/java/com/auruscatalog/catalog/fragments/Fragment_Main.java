package com.auruscatalog.catalog.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.ListFragment;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.DataController;
import com.auruscatalog.catalog.model.Slide;
import com.auruscatalog.catalog.slider.JazzyViewPager;
import com.auruscatalog.catalog.slider.SliderAdapter;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * Created by sparrow on 13.10.15.
 */
public class Fragment_Main extends Fragment {


    private CatalogApp app;

    private DataController mDataController;

    public static Fragment_Main getNewInstanse()
    {
        return new Fragment_Main();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        View main_view = inflater.inflate(R.layout.fragment_main, container, false);

        app = CatalogApp.getInstanse();
        mDataController = app.getDataController();
        setJazzy((JazzyViewPager) main_view.findViewById(R.id.jazzy_pager));
        return main_view;
    }
    private void setJazzy(JazzyViewPager jazzy)
    {
        jazzy.setTransitionEffect(JazzyViewPager.TransitionEffect.Stack);
        jazzy.setPageMargin(30);

        ArrayList<Map<String, Object>> data = new ArrayList<>();

        Slide[] slides = mDataController.getSlides();

        for (int i = 0; i < app.getDataController().getSlideCount(); i++) {
            addData(
                    data,
                    slides[i].getType(),
                    slides[i].getImageUrlId(),
                    slides[i].getType() == Slide.Type.Group ? slides[i].getGroupId() : slides[i].getProductId());
        }

        jazzy.setAdapter(new SliderAdapter(getActivity().getApplicationContext(), jazzy, data));
    }

    private void addData(ArrayList<Map<String, Object>> data, Slide.Type type, int image, int value)
    {
        Map<String, Object> m = new HashMap<>();
        m.put("type", type);
        m.put("value", value);
        m.put("image", image);
        data.add(m);
    }

}
