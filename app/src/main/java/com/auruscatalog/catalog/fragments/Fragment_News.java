package com.auruscatalog.catalog.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ListFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.News;
import com.auruscatalog.catalog.server.ServerApi;
import com.auruscatalog.catalog.server.ServerApiFabric;
import com.auruscatalog.catalog.utility.Utility;
import com.squareup.picasso.Picasso;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sparrow on 13.10.15.
 */
public class Fragment_News extends ListFragment {


    private class ListAdapter extends BaseAdapter
    {
        private News[] mNews;
        private Context mContext;

        private LayoutInflater mInflater;

        public ListAdapter(Context context, News[] news)
        {
            mNews = news;
            mContext = context;

            mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mNews.length != 0 ? mNews.length : 1;
        }

        @Override
        public Object getItem(int position) {
            return mNews.length != 0 ? mNews[position] : null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = mInflater.inflate(R.layout.news_card, parent, false);

            News currentNews = (News)getItem(position);

            if(currentNews != null) {
                ((TextView) convertView.findViewById(R.id.news_title)).setText(currentNews.getTitle());
                ((TextView) convertView.findViewById(R.id.news_author)).setText(currentNews.getAuthor());
                ((TextView) convertView.findViewById(R.id.news_date)).setText(currentNews.getDate());


                ((TextView) convertView.findViewById(R.id.news_content)).setText(currentNews.getContent());

                ImageView imageView = (ImageView) convertView.findViewById(R.id.news_img);

                Picasso.with(mContext)
                        .load(currentNews.getImageUrl())
                        .resize(100, 100)
                        .into(imageView);
            }

            return convertView;
        }
    }

    TextView mEmptyText;

    ProgressBar mProgressBar;

    private CatalogApp mCatalogApp;

    public static Fragment_News newInstance(CatalogApp catalogApp)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("app", catalogApp);

        Fragment_News news = new Fragment_News();
        news.setArguments(bundle);

        return news;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if(getArguments() != null)
        {
            mCatalogApp = (CatalogApp)getArguments().getSerializable("app");
        }

        ServerApi serverApi = ServerApiFabric.createRetrofitService(ServerApi.class, ServerApi.END_POINT);

        serverApi.getNews(Integer.toString(mCatalogApp.getDataController().getLastIdByNews()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<News[]>() {
                    @Override
                    public void onCompleted() {
                        News[] mNews = mCatalogApp.getDataController().getNews();
                        mProgressBar.setVisibility(View.GONE);
                        if(mNews == null || mNews.length == 0)
                        {
                            mEmptyText.setVisibility(View.VISIBLE);
                            getListView().setVisibility(View.GONE);
                        }
                        else {
                            ListAdapter listAdapter = new ListAdapter(getActivity().getApplicationContext(), mNews);
                            setListAdapter(listAdapter);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        String str = "nob";
                    }

                    @Override
                    public void onNext(News[] newses) {
                        mCatalogApp.getDataController().setNews(newses);
                    }
                });

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View list = inflater.inflate(R.layout.fragment_news, container, false);

        mEmptyText = (TextView)list.findViewById(R.id.empty);
        mProgressBar = (ProgressBar)list.findViewById(R.id.loading_spinner);

      //  mShortAnimation = getResources().getInteger(android.R.integer.config_shortAnimTime);

        return list;
    }


}
