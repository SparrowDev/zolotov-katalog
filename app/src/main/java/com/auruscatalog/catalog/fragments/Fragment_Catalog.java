package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.CatalogEnity;
import com.auruscatalog.catalog.model.Complekt;
import com.auruscatalog.catalog.model.DataController;
import com.auruscatalog.catalog.model.Group;
import com.auruscatalog.catalog.model.Product;
import com.auruscatalog.catalog.model.Size;
import com.auruscatalog.catalog.server.ServerApi;
import com.auruscatalog.catalog.server.ServerApiFabric;
import com.auruscatalog.catalog.utility.Utility;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sparrow on 21.10.15.
 */
public class Fragment_Catalog extends Fragment{

    private int mShortAnimation;

    private class CalculationNumberColumn
    {
        public void getNum(View view, ViewGroup parent)
        {
            View item = LayoutInflater.from(getActivity().getApplicationContext())
                    .inflate(R.layout.imagetextcell, parent);
            int i = view.getLayoutParams().width / (item.getLayoutParams().width + 20);
        }
    }

    /**
     * Класс отвечающий за то как будут располагаться элементы в RecyclerView
     */
    private class GridManager extends GridLayoutManager
    {
        public GridManager(Context context, int spanCount) {
            super(context, spanCount);
        }

    }

    private enum TypeItemAdapter
    {
        Group,
        Product,
        Complekt
    }

    public class CatalogRecyclerAdpater extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        class CatalogItem extends RecyclerView.ViewHolder
        {
            ImageView mImageView;
            TextView mTextView;

            TextView mPriceView;

            public CatalogItem(View itemView) {
                super(itemView);

                mImageView = (ImageView)itemView.findViewById(R.id.imageView);

                mTextView = (TextView)itemView.findViewById(R.id.textView);
                mPriceView = (TextView)itemView.findViewById(R.id.textPrice);
            }
        }

        ArrayList<CatalogEnity> mContent;
        TypeItemAdapter mTypeItemAdapter;

        public CatalogRecyclerAdpater(CatalogEnity content)
        {

        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 0;
        }
    }

    public class CatalogGridAdapter extends BaseAdapter
    {
        LayoutInflater mLayoutInflater;

        Context mContext;

        ArrayList<CatalogEnity> mContent;
        TypeItemAdapter mTypeItemAdapter;

        public CatalogGridAdapter(Context context, TypeItemAdapter typeItemAdapter)
        {
            mLayoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mContext = context;

            mTypeItemAdapter = typeItemAdapter;
        }

        public void newData(CatalogEnity[] content)
        {
            mContent = new ArrayList<>();

            for(CatalogEnity cont : content)
            {
                mContent.add(cont);
            }

            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mContent.size();
        }

        @Override
        public CatalogEnity getItem(int position) {
            return mContent.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = mLayoutInflater.inflate(R.layout.imagetextcell, parent, false);

            TextView textView = (TextView)convertView.findViewById(R.id.textView);
            TextView priceView = (TextView)convertView.findViewById(R.id.textPrice);
            ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView);

            CatalogEnity temp = getItem(position);

            textView.setText(temp.getName());

            //Если отображаются товары то показваем их цену
            if(mTypeItemAdapter == TypeItemAdapter.Product) {
                priceView.setText(Integer.toString(mDataController.getAveragePriceProduct(temp.getId())) + " р.");
            }
            else if(mTypeItemAdapter == TypeItemAdapter.Complekt)
            {
                priceView.setText(mDataController.getPriceComplekt(temp.getId()));
            }

            String url = mCatalogApp.getDataController().getImageUrl(temp.getImageUrlId());

            if(url != null && !url.equals("") && !url.equals(" ")) {
                Picasso
                        .with(mContext)
                        .load(url)
                        .into(imageView);
            }

            return convertView;
        }
    }

    private CatalogApp mCatalogApp;

    private GridView mGridView;

    private DataController mDataController;

    private CatalogGridAdapter mCatalogGridAdapter;

    private ServerApi mServerApi;

    private TextView mEmptyText;

    private ProgressBar mProgressBar;

    public static Fragment_Catalog newInstance(CatalogApp catalogApp, boolean complekt)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("app", catalogApp);

        bundle.putBoolean("complekt", complekt);

        Fragment_Catalog catalog = new Fragment_Catalog();
        catalog.setArguments(bundle);

        return catalog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        boolean mComplekt = false;
        String group_id = null;
        int parent_id = -1;

        if(getArguments() != null)
        {
            mCatalogApp = (CatalogApp)getArguments().getSerializable("app");
            mComplekt = getArguments().getBoolean("complekt");

            if(getArguments().getString("group_id") != null)
                group_id = getArguments().getString("group_id");
        }

        if(savedInstanceState != null)
        {
            if(savedInstanceState.getString("group_id") != null)
                group_id = savedInstanceState.getString("group_id");
            else if(savedInstanceState.getString("parent_id") != null)
                parent_id = Integer.parseInt(savedInstanceState.getString("parent_id"));
        }

        mDataController = mCatalogApp.getDataController();

        Utility.crossfade(mProgressBar, mShortAnimation);

        mServerApi = ServerApiFabric.createRetrofitService(ServerApi.class, ServerApi.END_POINT);

        if(group_id != null)
        {
            setOnProduct(group_id);
        }
        else if(!mComplekt) {
            setOnGroup(parent_id);
        }
        else {
            setOnComplekt();
        }

        mGridView.setOnItemClickListener(mItemClickListener);

        super.onActivityCreated(savedInstanceState);
    }

    private void setOnProduct(String group_id)
    {
        CatalogEnity[] data = mDataController.getProductByGroup(Integer.parseInt(group_id));

        mCatalogGridAdapter = new CatalogGridAdapter(getActivity().getApplicationContext(), TypeItemAdapter.Product);
        mCatalogGridAdapter.newData(data);

        mGridView.setAdapter(mCatalogGridAdapter);

        final String finalGroup_id = group_id;
        mServerApi.getProduct(Integer.toString(mDataController.getLastRevisionByProduct()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Product[]>() {
                    @Override
                    public void onCompleted() {
                        Product[] products = mDataController.getProductByGroup(Integer.parseInt(finalGroup_id));
                        if(products != null && products.length > 0)
                            mCatalogGridAdapter.newData(products);
                        else
                            onEmptyShow();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Product[] products) {
                        mDataController.setProducts(products);
                    }
                });
    }

    private void setOnGroup(final int parent)
    {
        mServerApi.getGroup(Integer.toString(mDataController.getLastRevisionByGroup()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Group[]>() {
                    @Override
                    public void onCompleted() {
                        CatalogEnity[] data = mDataController.getGroupByParent(parent);

                        if(data != null && data.length > 0) {
                            mCatalogGridAdapter = new CatalogGridAdapter(getActivity().getApplicationContext(), TypeItemAdapter.Group);
                            mCatalogGridAdapter.newData(data);
                            mGridView.setAdapter(mCatalogGridAdapter);
                            onProductUpdate();
                        }
                        else
                            onEmptyShow();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Group[] groups) {
                        mDataController.setGroups(groups);
                    }
                });
    }

    private void setOnComplekt()
    {
        mServerApi.getComplekt(Integer.toString(mDataController.getLastRevisionByComplekt()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Complekt[]>() {
                    @Override
                    public void onCompleted() {
                        CatalogEnity[] data = mDataController.getComplekts();
                        mCatalogGridAdapter = new CatalogGridAdapter(getActivity().getApplicationContext(),TypeItemAdapter.Complekt);
                        if(data != null && data.length > 0)
                        {
                            mCatalogGridAdapter.newData(data);
                            mGridView.setAdapter(mCatalogGridAdapter);
                            onProductUpdate();
                        }
                        else
                            onEmptyShow();

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Complekt[] complekts) {
                        mDataController.setComplekts(complekts);
                    }
                });
    }

    private void onProductUpdate()
    {
        mServerApi.getProduct(Integer.toString(mDataController.getLastRevisionByProduct()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Product[]>() {
                    @Override
                    public void onCompleted() {
                        mCatalogGridAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Product[] products) {
                        mDataController.setProducts(products);
                    }
                });
    }

    /**
     * Показвыает надпись об отсутствии данных
     */
    private void onEmptyShow()
    {
        mEmptyText.setVisibility(View.VISIBLE);
        mGridView.setVisibility(View.GONE);
    }

    /**
     * Производит возврат в каталоге на уровень выше
     * @return Возвращает false если возврат невозможне
     */
    public boolean onBack()
    {
        if(mCatalogGridAdapter == null)
            return false;
        if(mCatalogGridAdapter.mTypeItemAdapter == TypeItemAdapter.Group)
        {
            int parent_id = ((Group) mCatalogGridAdapter.getItem(0)).getParentId();

            if(parent_id != -1) {
                int parent = mDataController.getGroupParentByGroupId(parent_id);
                CatalogEnity[] data = mDataController.getGroupByParent(parent);
                mCatalogGridAdapter.newData(data);
                return true;
            }
            else
                return false;
        }
        else
        {
            int parent_id = ((Product) mCatalogGridAdapter.getItem(0)).getIdGroup();
            int parent = mDataController.getGroupParentByGroupId(parent_id);
            CatalogEnity[] data = mDataController.getGroupByParent(parent);

            mCatalogGridAdapter.mTypeItemAdapter = TypeItemAdapter.Group;
            mCatalogGridAdapter.newData(data);

            return true;
        }
    }

    private AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            CatalogEnity[] data = new CatalogEnity[0];
            switch (mCatalogGridAdapter.mTypeItemAdapter)
            {
                case Group:
                    data = mDataController.getGroupByParent(mCatalogGridAdapter.getItem(position).getId());

                    if(data.length == 0)
                    {
                        mCatalogGridAdapter.mTypeItemAdapter = TypeItemAdapter.Product;

                        mServerApi.getParametr(Integer.toString(mDataController.getLastRevisionBySize()))
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Observer<Size[]>() {
                                    @Override
                                    public void onCompleted() {
                                        mServerApi.getProduct(Integer.toString(mDataController.getLastRevisionByProduct()))
                                                .subscribeOn(Schedulers.newThread())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(new Observer<Product[]>() {
                                                    @Override
                                                    public void onCompleted() {
                                                        Product[] products = mDataController.getProductByGroup(mCatalogGridAdapter.getItem(position).getId());
                                                        if(products != null && products.length > 0)
                                                            mCatalogGridAdapter.newData(products);
                                                        else
                                                        {
                                                            onEmptyShow();
                                                        }
                                                    }

                                                    @Override
                                                    public void onError(Throwable e) {
                                                        e.printStackTrace();
                                                    }

                                                    @Override
                                                    public void onNext(Product[] products) {
                                                        mDataController.setProducts(products);
                                                    }
                                                });
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        e.printStackTrace();
                                    }

                                    @Override
                                    public void onNext(Size[] sizes) {
                                        mDataController.setSizes(sizes);
                                    }
                                });
                    }
                    else {
                        mCatalogGridAdapter.newData(data);
                    }
                    break;
                case Product:
                    ((ActivityMain)getActivity()).onShowProductCard(mDataController.getProduct(mCatalogGridAdapter.getItem(position).getId()));
                    break;
                case Complekt:
                    data = mDataController.getProductByComplekt(mCatalogGridAdapter.getItem(position).getId());
                    if(data == null && data.length > 0)
                        mCatalogGridAdapter.newData(data);
                    else
                        onEmptyShow();
                    break;
            }
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(mCatalogGridAdapter == null)
            return;

        if (mCatalogGridAdapter.mTypeItemAdapter == TypeItemAdapter.Group)
        {
            outState.putString("parent_id", Integer.toString(((Group) mCatalogGridAdapter.getItem(0)).getParentId()));
        }
        else if(mCatalogGridAdapter.mTypeItemAdapter == TypeItemAdapter.Product)
        {
            outState.putString("group_id", Integer.toString(((Product) mCatalogGridAdapter.getItem(0)).getIdGroup()));
        }
        else
        {

        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View catalog = inflater.inflate(R.layout.fragment_catalog ,container, false);

        RecyclerView recyclerView = new RecyclerView(getActivity().getApplicationContext());

        RecyclerView.ItemDecoration itemDecoration = new RecyclerView.ItemDecoration() {
            @Override
            public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
                super.onDraw(c, parent, state);
            }
        };

        mGridView = (GridView)catalog.findViewById(R.id.gridView);

        mEmptyText = (TextView)catalog.findViewById(R.id.empty);

        mProgressBar = (ProgressBar)catalog.findViewById(R.id.loading_spinner);

        mShortAnimation = getResources().getInteger(android.R.integer.config_shortAnimTime);

       /* CalculationNumberColumn numberColumn = new CalculationNumberColumn();
        numberColumn.getNum(catalog, container);*/

        return catalog;
    }
}
