package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.utility.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sparrow on 17.11.15.
 */
public class Fragment_Info extends Fragment {

    public enum TypeInfo implements Serializable
    {
        About,
        Parther,
        Contac
    }

    public class InfoRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        class AboutInfo extends RecyclerView.ViewHolder
        {
            TextView mTextLeft;
            TextView mTextRight;
            public AboutInfo(View itemView) {
                super(itemView);

                mTextLeft = (TextView)itemView.findViewById(R.id.textLeft);
                mTextRight = (TextView)itemView.findViewById(R.id.textRight);
            }

            public void setContent(String content,int position)
            {
                if(position % 2 == 0)
                    mTextLeft.setText(content);
                else
                    mTextRight.setText(content);
            }
        }

        class ContactInfo extends RecyclerView.ViewHolder
        {
            public ContactInfo(View itemView) {
                super(itemView);
            }
        }

        class ParthnerInfo extends RecyclerView.ViewHolder
        {
            public ParthnerInfo(View itemView) {
                super(itemView);
            }
        }

        List<String> mData;

        TypeInfo mTypeInfo;

        public InfoRecyclerAdapter(TypeInfo typeInfo)
        {
            mData = new ArrayList<>();
            mTypeInfo = typeInfo;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            int id = 0;

            if(mTypeInfo == TypeInfo.About)
                id = R.layout.item_about;
            else if(mTypeInfo == TypeInfo.Contac)
                id = R.layout.item_contact;
            else if(mTypeInfo == TypeInfo.Parther)
                id = R.layout.item_parther;
            View view = LayoutInflater.from(getActivity().getApplicationContext()).inflate(id, parent, false);
            return new AboutInfo(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if(mTypeInfo == TypeInfo.About)
                ((AboutInfo)holder).setContent(mData != null && position < mData.size() ? mData.get(position) : "", position);
        }

        @Override
        public int getItemCount() {
            return mData != null ? mData.size() : 0;
        }

        public void addItem(String content)
        {
            mData.add(content);
        }
    }

    public static Fragment_Info newInstanse(TypeInfo typeInfo)
    {
        Fragment_Info fragmentInfo = new Fragment_Info();

        Bundle bundle = new Bundle();
        bundle.putSerializable("type", typeInfo);

        fragmentInfo.setArguments(bundle);

        return fragmentInfo;
    }

    RecyclerView mRecyclerView;

    InfoRecyclerAdapter mAdapter;

    private ProgressBar mProgressBar;

    TypeInfo mTypeInfo;

    String[] mData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null)
        {
            mTypeInfo = (TypeInfo)getArguments().getSerializable("type");
        }

        mData = getResources().getString(R.string.info).split("\n");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);

        TextView mEmptyText = (TextView)view.findViewById(R.id.empty);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(layoutManager);

        mProgressBar = (ProgressBar)view.findViewById(R.id.loading_spinner);
        Utility.crossfade(mProgressBar, getResources().getInteger(android.R.integer.config_shortAnimTime));

        mAdapter = new InfoRecyclerAdapter(mTypeInfo);

        mRecyclerView.setAdapter(mAdapter);
        if(mData != null && mData.length > 0)
        {
            for(String s : mData)
            {
                mAdapter.addItem(s);
            }
            mEmptyText.setVisibility(View.GONE);
        }
        else
        {
            mRecyclerView.setVisibility(View.GONE);
        }


        return view;
    }
}
