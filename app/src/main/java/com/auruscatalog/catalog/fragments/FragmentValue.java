package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.Basket;
import com.auruscatalog.catalog.model.BasketPosition;
import com.auruscatalog.catalog.model.DataController;
import com.auruscatalog.catalog.model.Product;
import com.auruscatalog.catalog.model.Size;
import com.auruscatalog.catalog.server.ServerApi;
import com.auruscatalog.catalog.server.ServerApiFabric;
import com.auruscatalog.catalog.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sparrow on 02.11.15.
 */
public class FragmentValue extends Fragment {

    public class RecyclerSizeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {
        public class SizeHolder extends RecyclerView.ViewHolder
        {
            TextView mSize;

            TextView mWeight;

            TextView mPrice;

            Button mPlus;

            TextView mValue;

            Button mMinuse;

            public SizeHolder(View itemView) {
                super(itemView);

                mSize = (TextView)itemView.findViewById(R.id.csname);
                mPrice = (TextView)itemView.findViewById(R.id.csprice);
                mWeight = (TextView)itemView.findViewById(R.id.csweight);

                mPlus = (Button)itemView.findViewById(R.id.csplus);
                mValue = (TextView)itemView.findViewById(R.id.csvalue);
                mMinuse = (Button)itemView.findViewById(R.id.csminus);

                mPlus.setOnClickListener(onPlus);
                mMinuse.setOnClickListener(onMinuse);
            }

            public void setValue(String value)
            {
                if(Integer.parseInt(value) >= 0)
                    mValue.setText(value);

                if(Integer.parseInt(value) == 0)
                    mMinuse.setEnabled(false);
                else
                    mMinuse.setEnabled(true);
            }

            public void setWeight(String weight)
            {
                mWeight.setText(weight + " гр.");
            }

            public void setSize(String size)
            {
                mSize.setText(size);
            }

            public void setPrice(String price)
            {
                mPrice.setText(price + " р.");
            }

            public void setPosition(int position)
            {
                mPlus.setTag(position);
                mValue.setTag(position);
                mMinuse.setTag(position);
            }
        }

        private class Content
        {
            Size mSize;

            Integer mCount;

            public Content(Size size, int count)
            {
                mSize = size;

                mCount = count;
            }

            public Size getSize() {
                return mSize;
            }

            public Integer getCount() {
                return mCount;
            }

            public void setCount(Integer count) {
                mCount = count;
            }
        }

        private Content[] mData;

        public RecyclerSizeAdapter(Size[] sizes)
        {
            mData = new Content[sizes.length];

            for(int i = 0; i<sizes.length; i++)
            {
                mData[i] = new Content(sizes[i], 0);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SizeHolder(LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.card_size_item, parent, false));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            SizeHolder sizeHolder = (SizeHolder)holder;

            sizeHolder.setPrice(Double.toString(mData[position].getSize().getPrice()));
            sizeHolder.setSize(Double.toString(mData[position].getSize().getSize()));
            sizeHolder.setWeight(Double.toString(mData[position].getSize().getWeight()));
            sizeHolder.setValue(Integer.toString(mData[position].getCount()));

            sizeHolder.setPosition(position);
        }

        @Override
        public int getItemCount() {
            return mData.length;
        }

        public void newData(Size[] sizes)
        {
            Content[] data = new Content[sizes.length];

            for(int i = 0; i<sizes.length; i++)
            {
                data[i] = new Content(sizes[i], 0);
            }

            for(int i = 0; i < data.length; i++)
            {
                for(int j = 0; j < mData.length; j++)
                {

                }
            }
            notifyDataSetChanged();
        }

        public void onUpdate(int position, int value)
        {
            mData[position].setCount(value);
            notifyDataSetChanged();
        }

        public void onUpdateFromBasket(BasketPosition[] positions)
        {
            if(positions == null)
                return;

            for(BasketPosition position : positions)
            {
                for(Content content : mData)
                {
                    if(content.getSize().getId() == position.getSize().getId())
                    {
                        content.mCount = position.getCount();
                        break;
                    }
                }
            }
            notifyDataSetChanged();
        }

        public void onPlus(int position)
        {
            mData[position].setCount(mData[position].getCount() + 1);
            notifyDataSetChanged();
        }

        public void onMinuse(int position)
        {
            mData[position].setCount(mData[position].getCount() - 1);
            notifyDataSetChanged();
        }

        public BasketPosition[] getContent()
        {
            List<BasketPosition> result = new ArrayList<>();

            for(Content content : mData)
                if(content.mCount > 0)
                    result.add(new BasketPosition(mProduct, content.getSize(), content.getCount()));

            return result.toArray(new BasketPosition[result.size()]);
        }
    }

    private CatalogApp mCatalogApp;

    private Product mProduct;

    private RecyclerView mRecyclerView;

    private Size[] mSizes;

    private RecyclerSizeAdapter mSizeAdapter;

    private DataController mDataController;

    public static FragmentValue newInstanse(CatalogApp catalogApp, Product product)
    {
        Bundle bundle = new Bundle();

        bundle.putSerializable("app", catalogApp);
        bundle.putSerializable("product", product);

        FragmentValue fragmentValue = new FragmentValue();
        fragmentValue.setArguments(bundle);

        return fragmentValue;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null)
        {
            savedInstanceState = getArguments();
            if(savedInstanceState.getSerializable("app") != null)
                mCatalogApp = (CatalogApp)savedInstanceState.getSerializable("app");
            if(savedInstanceState.getSerializable("product") != null)
                mProduct = (Product)savedInstanceState.getSerializable("product");
        }

        mDataController = mCatalogApp.getDataController();

        mSizes = mDataController.getSize(mProduct.getId());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.fragment_size_list, container, false);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.list);

        initRecycler();

        Button add = (Button)view.findViewById(R.id.add_cart);

        add.setOnClickListener(addButton);

        return view;
    }

    private void initRecycler()
    {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);

        mSizeAdapter = new RecyclerSizeAdapter(mSizes);

        mRecyclerView.setAdapter(mSizeAdapter);

        ServerApi serverApi = ServerApiFabric.createRetrofitService(ServerApi.class, ServerApi.END_POINT);

        serverApi.getParametr(Integer.toString(mDataController.getLastRevisionBySize(mProduct.getId())))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Size[]>() {
                    @Override
                    public void onCompleted() {
                        mSizeAdapter.newData(mDataController.getSize(mProduct.getId()));
                        mSizeAdapter.onUpdateFromBasket(mCatalogApp.getBasket().getPositionByProduct(mProduct));
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Size[] sizes) {
                        mDataController.setSizes(sizes);
                    }
                });
    }

    View.OnClickListener onPlus = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mSizeAdapter.onPlus((int)v.getTag());
        }
    };

    View.OnClickListener onMinuse = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mSizeAdapter.onMinuse((int)v.getTag());
        }
    };

    View.OnClickListener addButton = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            BasketPosition[] basketPositions = mSizeAdapter.getContent();

            if(basketPositions == null || basketPositions.length == 0)
            {
                Toast.makeText(getActivity().getApplicationContext(),
                        "Нечего добавлять",
                        Toast.LENGTH_SHORT).show();
            }
            else {
                Basket.UpdatePosition updatePosition = mCatalogApp.getBasket().onAddPosition(basketPositions);

                Toast.makeText(getActivity().getApplicationContext(),
                        updatePosition.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        }
    };
}
