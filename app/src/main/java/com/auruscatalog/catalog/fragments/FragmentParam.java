package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.DataController;
import com.auruscatalog.catalog.model.Dictonary;
import com.auruscatalog.catalog.model.Product;
import com.auruscatalog.catalog.server.ServerApi;
import com.auruscatalog.catalog.server.ServerApiFabric;
import com.squareup.picasso.Picasso;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by sparrow on 01.11.15.
 */
public class FragmentParam extends Fragment
{
    public class RecyclerCardAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
    {

        public class HeaderHolder extends RecyclerView.ViewHolder {

            private ImageView mImageView;

            public HeaderHolder(View itemView) {
                super(itemView);

                mImageView = (ImageView)itemView.findViewById(R.id.imageView);
                mImageView.setOnClickListener(mParent.zoomClick);

                mImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ActivityMain)getActivity()).onShowImageViewCard(v.getTag().toString());
                    }
                });
            }

            public void setImage(String image)
            {
                mImageView.setTag(image);
                Picasso
                        .with(getActivity().getApplicationContext())
                        .load(image)
                        .into(mImageView);
            }
        }

        public class ItemParamHolder extends RecyclerView.ViewHolder
        {
            private TextView mParam;

            private TextView mValue;

            public ItemParamHolder(View itemView) {
                super(itemView);

                mParam = (TextView)itemView.findViewById(R.id.card_desc_item);
                mValue = (TextView)itemView.findViewById(R.id.card_desc_value);
            }

            public void setParam(String text)
            {
                mParam.setText(text);
            }

            public void setValue(String value)
            {
                mValue.setText(value);
            }

        }

        FragmentParam mParent;
        Dictonary[] mParams;

        public RecyclerCardAdapter(FragmentParam parent, Dictonary[] params)
        {
            mParent = parent;
            mParams = params;
        }

        public void setParams(Dictonary[] params)
        {
            mParams = params;
            notifyDataSetChanged();
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if(viewType == 0)
            {
                return new HeaderHolder(LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.header_card, parent, false));
            }
            else
            {
                return new ItemParamHolder(LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.card_desc_item, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if(position == 0)
            {
                ((HeaderHolder)holder).setImage(mDataController.getImageUrl(mParent.mProduct.getImageUrlId()));
            }
            else
            {
                ItemParamHolder item = (ItemParamHolder)holder;
                item.setParam(mParams[position - 1].getName());
                item.setValue(mParams[position - 1].getValue());
            }
        }

        @Override
        public int getItemViewType(int position) {
            int result = 0;

            if(position > 0)
                result = 1;

            return result;
        }

        @Override
        public int getItemCount() {
            return mParams.length + 1;
        }
    }

    Product mProduct;

    CatalogApp mCatalogApp;

    DataController mDataController;

    RecyclerView mRecyclerView;

    public static FragmentParam newInstanse(CatalogApp app, Product product)
    {
        Bundle bundle = new Bundle();

        bundle.putSerializable("app", app);
        bundle.putSerializable("product", product);

        FragmentParam fragmentCard = new FragmentParam();

        fragmentCard.setArguments(bundle);

        return fragmentCard;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null)
        {
            savedInstanceState = getArguments();
            if(savedInstanceState.getSerializable("app") != null)
                mCatalogApp = (CatalogApp)savedInstanceState.getSerializable("app");

            if(savedInstanceState.getSerializable("product") != null)
                mProduct = (Product)savedInstanceState.getSerializable("product");
        }

        mDataController = mCatalogApp.getDataController();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity().getApplication()).inflate(R.layout.fragment_param_list, container, false);

        mRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);

        initRecycler();

        return view;
    }

    public void initRecycler()
    {
        final RecyclerCardAdapter mCardAdapter = new RecyclerCardAdapter(this, mDataController.getDictonaryByProduct(mProduct.getId()));
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mCardAdapter);

        ServerApi serverApi = ServerApiFabric.createRetrofitService(ServerApi.class, ServerApi.END_POINT);

        serverApi.getParamList(
                        Integer.toString(mProduct.getId()),
                        Integer.toString(mDataController.getLastRevisionByDictonary(mProduct.getId()))
                              )
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Dictonary[]>() {
                    @Override
                    public void onCompleted() {
                        mCardAdapter.setParams(mDataController.getDictonaryByProduct(mProduct.getId()));
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Dictonary[] dictonaries) {
                        mDataController.setDictonary(dictonaries);
                    }
                });
    }

    View.OnClickListener zoomClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ((ActivityMain)getActivity()).onZoom(mDataController.getImageUrl(mProduct.getImageUrlId()));
        }
    };
}
