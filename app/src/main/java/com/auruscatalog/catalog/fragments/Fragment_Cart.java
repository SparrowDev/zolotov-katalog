package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.Basket;
import com.auruscatalog.catalog.model.BasketPosition;
import com.auruscatalog.catalog.utility.Utility;
import com.squareup.picasso.Picasso;

import java.io.Serializable;

/**
 * Created by sparrow on 06.11.15.
 */
public class Fragment_Cart extends Fragment{

    public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.BasketHolder>
    {
        public class BasketHolder extends RecyclerView.ViewHolder implements Serializable
        {
            Button mPlus;
            Button mMinuse;

            ImageView mImageView;

            TextView mName;
            TextView mTotalPrice;

            TextView mCount;

            TextView mWeight;
            TextView mSize;
            TextView mPrice;

            public BasketHolder(View itemView) {
                super(itemView);

                mPlus = (Button)itemView.findViewById(R.id.btn_plus);
                mMinuse = (Button)itemView.findViewById(R.id.btn_minus);

                mImageView = (ImageView)itemView.findViewById(R.id.image);

                mName = (TextView)itemView.findViewById(R.id.productName);
                mTotalPrice = (TextView)itemView.findViewById(R.id.totalPrice);
                mCount = (TextView)itemView.findViewById(R.id.text_count_value);
                mSize = (TextView)itemView.findViewById(R.id.sizeProduct);
                mWeight = (TextView)itemView.findViewById(R.id.weightProduct);
                mPrice = (TextView)itemView.findViewById(R.id.priceProduct);

                mPlus.setOnClickListener(clickPlus);
                mMinuse.setOnClickListener(clickMinuse);
            }

            public void setImage(String url)
            {
                Picasso
                        .with(getActivity().getApplicationContext())
                        .load(url)
                        .placeholder(R.drawable.default_image_background)
                        .into(mImageView);
            }

            public void setName(String name)
            {
                mName.setText(name);
            }

            public void setCount(String count)
            {
                mCount.setText(count);
            }

            public void setTotalPrice(String total)
            {
                mTotalPrice.setText(Utility.reformatPrice(Double.parseDouble(total)));
            }

            public void setWeight(String weight)
            {
                mWeight.setText(weight);
            }

            public void setPrice(String price)
            {
                mPrice.setText(price);
            }

            public void setSize(String size)
            {
                mSize.setText(size);
            }

            public void setTag(int position)
            {
                mPlus.setTag(position);
                mMinuse.setTag(position);
            }

            public double getPrice()
            {
                return
                        Double.parseDouble(mPrice.getText().toString()) *
                                Double.parseDouble(mCount.getText().toString());
            }
        }

        Basket mBasket;

        public CartRecyclerAdapter(Basket basket)
        {
            mBasket = basket;
        }

        @Override
        public BasketHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new BasketHolder(LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.cart_list_item, parent, false));
        }

        @Override
        public void onBindViewHolder(BasketHolder holder, int position) {

            BasketPosition item = mBasket.getPosition(position);

            holder.setImage(item.getImageUrl());
            holder.setName(item.getProductName());
            holder.setPrice(Double.toString(item.getSize().getPrice()));
            holder.setTotalPrice(Double.toString(item.getPrice()));
            holder.setWeight(Double.toString(item.getSize().getWeight()));
            holder.setSize(Double.toString(item.getSize().getSize()));
            holder.setCount(Integer.toString(item.getCount()));

            holder.setTag(position);
        }

        @Override
        public int getItemCount() {
            return mBasket.getCount();
        }

        public View.OnClickListener clickPlus = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBasket.onAddToPosition((int)v.getTag(), true);
                notifyDataSetChanged();
            }
        };

        public View.OnClickListener clickMinuse = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBasket.onAddToPosition((int)v.getTag(), false);
                notifyDataSetChanged();
            }
        };
    }

    public static Fragment_Cart newInstanse()
    {
        return new Fragment_Cart();
    }

    private Basket mBasket;

    private ItemTouchHelper mSwipeToDismissTouchHelper;
    private RecyclerView recyclerView;
    private CartRecyclerAdapter mCartRecyclerAdapter;

    private Fragment_Cart mCurrentFragment;

    private TextView mEmptyText;

    private Button mOrderBtn;
    private Button mExportBtn;

    private TextView mTotalPrice;

    private TextView mCountGoods;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CatalogApp catalogApp = CatalogApp.getInstanse();

        mBasket = catalogApp.getBasket();

        mCurrentFragment = this;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = LayoutInflater.from(getActivity().getApplication()).inflate(R.layout.fragment_cart, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.list_cart);
        mEmptyText = (TextView)view.findViewById(R.id.empty);
        LinearLayout root = (LinearLayout)view.findViewById(R.id.rootLayout);
        mTotalPrice = (TextView)view.findViewById(R.id.totalPrice);
        mCountGoods = (TextView)view.findViewById(R.id.countGoods);

        if(mBasket == null || mBasket.getCount() == 0)
        {
            mEmptyText.setVisibility(View.VISIBLE);
            root.setVisibility(View.GONE);
        }
        else
        {
            mTotalPrice.setText(mTotalPrice.getText() + Double.toString(mBasket.getPrice()));
            mCountGoods.setText(mCountGoods.getText() + Integer.toString(mBasket.getCount()));
        }

        mOrderBtn = (Button)view.findViewById(R.id.order);
        mExportBtn = (Button)view.findViewById(R.id.export);

        mOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityMain)getActivity()).onToOrder();
            }
        });

        mExportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ActivityMain)getActivity()).onShowExportDialog();
            }
        });

        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mCartRecyclerAdapter = new CartRecyclerAdapter(mBasket);

        recyclerView.setAdapter(mCartRecyclerAdapter);
        recyclerView.setLayoutManager(layoutManager);

        mSwipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                // callback for drag-n-drop, false to skip this feature
                return false;
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                // callback for swipe to dismiss, removing item from data and adapter
                CartRecyclerAdapter.BasketHolder holder = (CartRecyclerAdapter.BasketHolder)viewHolder;

                DialogDrop dialogDrop = DialogDrop.newInstanse(holder);
                dialogDrop.setTargetFragment(mCurrentFragment, 0);

                ((ActivityMain)getActivity()).onShowDialogDropCartItem(dialogDrop);
            }
        });
        mSwipeToDismissTouchHelper.attachToRecyclerView(recyclerView);

        return view;
    }

    private View.OnClickListener onExportClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        boolean result = data.getBooleanExtra("result", false);
        RecyclerView.ViewHolder viewHolder = (RecyclerView.ViewHolder) data.getSerializableExtra("holder");

        if(result)
        {
            BasketPosition removed = mBasket.onRemovePositionByIndex(viewHolder.getAdapterPosition());
            Toast.makeText(
                    getActivity().getApplicationContext(),
                    "Товар удален. Цена снижена на " + Utility.reformatPrice(removed.getPrice()),
                    Toast.LENGTH_LONG).show();
            mCartRecyclerAdapter.notifyItemRemoved(viewHolder.getAdapterPosition());
        }
        mCartRecyclerAdapter.notifyDataSetChanged();
        if(mBasket.getCount() == 0 || mBasket == null)
        {
            mEmptyText.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity().getApplicationContext(), "Корзина пуста", Toast.LENGTH_SHORT).show();
        }
    }
}
