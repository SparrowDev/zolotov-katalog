package com.auruscatalog.catalog.fragments;

import android.app.Application;
import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.Basket;
import com.auruscatalog.catalog.utility.Utility;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PaymentActivity;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileReader;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by sparrow on 23.11.15.
 */
public class Fragment_Order extends Fragment {

    public static Fragment_Order newInstanse()
    {
        Fragment_Order mFragment = new Fragment_Order();

        return mFragment;
    }

    private Basket mBasket;

    Button btn_pay;
    Button btn_export;

    EditText et_name;
    EditText et_phone;
    EditText et_email;
    EditText et_note;

    public Fragment_Order()
    {
        mBasket = CatalogApp.getInstanse().getBasket();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order, container, false);

        et_name = (EditText)view.findViewById(R.id.et_fio);
        et_phone = (EditText)view.findViewById(R.id.et_phone);
        et_email = (EditText)view.findViewById(R.id.et_email);
        et_note = (EditText)view.findViewById(R.id.et_note);

        btn_pay = (Button)view.findViewById(R.id.btn_order);
        btn_export = (Button)view.findViewById(R.id.btn_send);

        btn_pay.setOnClickListener(pay);
        return view;
    }

    View.OnClickListener pay = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Double total_price = Basket.getInstanse().getPrice();
            PayPalPayment payment = new PayPalPayment(new BigDecimal(total_price), "RUB", "Заказ Aurus",
                    PayPalPayment.PAYMENT_INTENT_SALE);

            Intent intent = new Intent(getActivity().getApplicationContext(), PaymentActivity.class);

            intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);

            startActivityForResult(intent, 0);
        }
    };

    View.OnClickListener sendEmail = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(et_email.getText().length() == 0 || et_name.getText().length() == 0 || et_phone.getText().length() == 0)
            {
                Toast.makeText(getActivity().getApplicationContext(), "Заполнены не все обязательные поля!", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Intent email = new Intent(Intent.ACTION_SEND);
                email.putExtra(Intent.EXTRA_EMAIL, new String[]{et_email.getText().toString()});
                email.putExtra(Intent.EXTRA_SUBJECT, "zolotov@yandex.ru");
                email.putExtra(Intent.EXTRA_TEXT, "Ваш заказ");

                try {
                    File xls = new File((new URI("file://" + Environment.getExternalStorageDirectory() + "/order.xls")));
                    if(xls.exists())
                    {
                        if(!xls.delete())
                            Toast.makeText(getActivity().getApplicationContext(), R.string.ErrorReplaceFileXls, Toast.LENGTH_LONG);
                        else
                        {
                            Utility.toXLS(
                                    "file://" + Environment.getExternalStorageDirectory() + "/order.xls",
                                    et_email.getText().toString(),
                                    et_name.getText().toString(),
                                    et_phone.getText().toString(),
                                    et_note.getText().toString());
                        }
                    }

                    if(!xls.exists())
                    {
                        Utility.toXLS(
                                "file://" + Environment.getExternalStorageDirectory() + "/order.xls",
                                et_email.getText().toString(),
                                et_name.getText().toString(),
                                et_phone.getText().toString(),
                                et_note.getText().toString());
                        if(!xls.exists())
                            Toast.makeText(getActivity().getApplicationContext(), "Файл xls не найден и не может быть создан. Письмо не будет отправлено", Toast.LENGTH_LONG).show();
                    }
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                email.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + Environment.getExternalStorageDirectory() + "/order.xls"));
                email.setType("message/rfc822");
                startActivity(Intent.createChooser(email, "Выберите email клиент:"));

            }
        }
    };

}
