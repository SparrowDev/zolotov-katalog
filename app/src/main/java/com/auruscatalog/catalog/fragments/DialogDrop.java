package com.auruscatalog.catalog.fragments;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.utility.Utility;


/**
 * Created by sparrow on 22.11.15.
 */
public class DialogDrop extends DialogFragment implements DialogInterface.OnClickListener
{
    double mPrice;

    Fragment_Cart.CartRecyclerAdapter.BasketHolder mHolder;

    public static DialogDrop newInstanse(Fragment_Cart.CartRecyclerAdapter.BasketHolder holder)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("holder", holder);

        DialogDrop dialogDrop = new DialogDrop();

        dialogDrop.setArguments(bundle);
        return dialogDrop;
    }

    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        if(getArguments() != null)
            mHolder = (Fragment_Cart.CartRecyclerAdapter.BasketHolder)getArguments().getSerializable("holder");

        mPrice = mHolder != null ? mHolder.getPrice() : 0;

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle("Удалить?")
                .setCancelable(false)
                .setPositiveButton("Удалить", this)
                .setNegativeButton("Отмена", this)
                .setMessage(getResources().getString(R.string.dropGoods) + Utility.reformatPrice(mPrice))
                .create();
        return alertDialog;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Intent result = new Intent();
        result.putExtra("holder", mHolder);

        switch (which)
        {
            case android.app.Dialog.BUTTON_POSITIVE:
                result.putExtra("result", true);
                break;
            case android.app.Dialog.BUTTON_NEGATIVE:
                result.putExtra("result", false);
                break;
        }
        getTargetFragment().onActivityResult(getTargetRequestCode(), 0, result);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }
}
