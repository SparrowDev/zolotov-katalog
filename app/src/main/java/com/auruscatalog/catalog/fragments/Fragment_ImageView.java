package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * Created by sparrow on 08.11.15.
 */
public class Fragment_ImageView extends Fragment {

    private String mImageUrl;

    private PhotoViewAttacher mPhotoView;

    public static Fragment_ImageView newInstanse(String imageUrl)
    {
        Bundle bundle = new Bundle();

        Fragment_ImageView fragmentImageView = new Fragment_ImageView();

        bundle.putString("url", imageUrl != null ? imageUrl : "");

        fragmentImageView.setArguments(bundle);

        return fragmentImageView;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments().getString("url") != null)
            mImageUrl = getArguments().getString("url");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity().getApplicationContext()).inflate(R.layout.cardpictureviewer, container, false);

        final ImageView imageView = (ImageView)view.findViewById(R.id.picture);

        if(mImageUrl != null && !mImageUrl.equals("")) {
            Picasso
                    .with(getActivity().getApplicationContext())
                    .load(mImageUrl)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            mPhotoView = new PhotoViewAttacher(imageView);
                        }

                        @Override
                        public void onError() {
                            Toast.makeText(getActivity().getApplicationContext(), "Ошибка при загрузке изображения", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        else
            Toast.makeText(getActivity().getApplicationContext(), "Изображение не найдено", Toast.LENGTH_SHORT).show();

        return view;
    }
}
