package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.activity.ActivityMain;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.Image;
import com.auruscatalog.catalog.model.Slide;
import com.auruscatalog.catalog.server.ServerApi;
import com.auruscatalog.catalog.server.ServerApiFabric;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by sparrow on 24.11.15.
 */
public class StartScreen extends Fragment {

    ServerApi mServerApi;

    public static StartScreen newInstanse()
    {
        StartScreen startScreen = new StartScreen();

        return startScreen;
    }

    public StartScreen()
    {
        mServerApi = ServerApiFabric.createRetrofitService(ServerApi.class, ServerApi.END_POINT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View start = inflater.inflate(R.layout.fragment_start, container, false);

        return start;
    }

    @Override
    public void onStart() {
        super.onStart();

        final CatalogApp app = CatalogApp.getInstanse();

        mServerApi.getSlide(Integer.toString(app.getDataController().getLastRevisionSlide()))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Slide[]>() {
                    @Override
                    public void onCompleted() {
                        mServerApi.getImage(Integer.toString(app.getDataController().getLastRevisionImage()))
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Observer<Image[]>() {
                                    @Override
                                    public void onCompleted() {
                                        onWaitStart(2500);
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                    }

                                    @Override
                                    public void onNext(Image[] image) {
                                        app.getDataController().setImages(image);
                                    }
                                });
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Slide[] slide) {
                        app.getDataController().setSlides(slide);
                    }
                });


    }

    /**
     * Запускает ожидание для показа стартового окна
     * @param latency Задержка ожидания в миллисекундах
     */
    private void onWaitStart(final long latency)
    {
        Thread thread = new Thread()
        {
            public void run()
            {
                try
                {
                    int logoTimer = 0;
                    while(logoTimer < latency)
                    {
                        sleep(100);
                        logoTimer = logoTimer +100;
                    }
                    while (getActivity() == null)
                        sleep(100);

                    ((ActivityMain) getActivity()).onStartMain();
                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}

