package com.auruscatalog.catalog.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.application.CatalogApp;
import com.auruscatalog.catalog.model.DataController;
import com.auruscatalog.catalog.model.Product;


/**
 * Created by sparrow on 01.11.15.
 */
public class Fragment_Card extends Fragment{

    public class FragmentPageAdapter extends android.support.v13.app.FragmentPagerAdapter
    {
        FragmentParam mFragmentParam;

        FragmentValue mFragmentValue;

        FragmentManager mFragmentManager;

        FragmentTransaction mCurrentTransaction;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            onRemoveOldFragment(position);

            if(mCurrentTransaction == null)
                mCurrentTransaction = mFragmentManager.beginTransaction();

            mCurrentTransaction
                    .add(container.getId(), getItem(position), makeNameItem(position));
            return getItem(position);
        }

        private void onRemoveOldFragment(int position)
        {
            if(mFragmentManager.findFragmentByTag(makeNameItem(position)) != null)
            {
                if(mCurrentTransaction == null)
                    mCurrentTransaction = mFragmentManager.beginTransaction();

                mCurrentTransaction
                        .remove(mFragmentManager.findFragmentByTag(makeNameItem(position)))
                        .commit();
                mCurrentTransaction = null;
            }
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            if(mCurrentTransaction != null)
            {
                mCurrentTransaction.commit();
                mCurrentTransaction = null;
            }
            super.finishUpdate(container);
        }

        public FragmentPageAdapter(android.app.FragmentManager fm, FragmentParam fragmentParam, FragmentValue fragmentValue) {
            super(fm);
            mFragmentManager = fm;

            mFragmentParam = fragmentParam;
            mFragmentValue = fragmentValue;
        }

        private String makeNameItem(int position)
        {
            String result = "";
            switch (position)
            {
                case 0:
                    result = "one";
                    break;
                case 1:
                    result = "two";
                    break;
            }

            return result;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0)
                return mFragmentParam;
            else
                return mFragmentValue;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0)
                return "Товар";
            else
                return "Размеры";
        }
    }

    private FragmentPagerAdapter mPagerAdapter;

    private ViewPager mViewPager;

    private CatalogApp mCatalogApp;

    private DataController mDataController;

    private Product mProduct;

    private FragmentParam mFragmentParam;

    private FragmentValue mFragmentValue;

    public static Fragment_Card newInstanse(CatalogApp app, Product product)
    {
        Bundle bundle = new Bundle();

        bundle.putSerializable("app", app);
        bundle.putSerializable("product", product);

        Fragment_Card fragmentCard = new Fragment_Card();

        fragmentCard.setArguments(bundle);

        return fragmentCard;
    }

    public Product getProduct()
    {
        return mProduct;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null)
        {
            savedInstanceState = getArguments();

            if(savedInstanceState.getSerializable("app") != null)
                mCatalogApp = (CatalogApp)savedInstanceState.getSerializable("app");

            if(savedInstanceState.getSerializable("product") != null)
                mProduct = (Product)savedInstanceState.getSerializable("product");
        }

        mDataController = mCatalogApp.getDataController();

        mFragmentParam = FragmentParam.newInstanse(mCatalogApp, mProduct);

        mFragmentValue = FragmentValue.newInstanse(mCatalogApp, mProduct);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        mViewPager = (ViewPager)view.findViewById(R.id.pager);

        mPagerAdapter = new FragmentPageAdapter(getFragmentManager(), mFragmentParam, mFragmentValue);

        mViewPager.setAdapter(mPagerAdapter);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public Fragment getParamFragment()
    {
        return mFragmentParam;
    }

    public Fragment getValueFragment()
    {
        return mFragmentValue;
    }

    /**
     * Обработчик нажатия кнопки Zoom
     */
    View.OnClickListener zoomClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
}
