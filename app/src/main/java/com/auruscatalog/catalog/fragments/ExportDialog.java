package com.auruscatalog.catalog.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.utility.Utility;

import java.io.File;

/**
 * Created by sparrow on 17.01.16.
 */
public class ExportDialog extends Fragment{

    EditText et_name;
    EditText et_phone;
    EditText et_email;
    EditText et_note;

    Button btn_cancel;
    Button btn_export;


    public static ExportDialog getInstanse()
    {
        ExportDialog exportDialog = new ExportDialog();
        return exportDialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View dialog = inflater.inflate(R.layout.export_dialog, container, false);

        et_name = (EditText)dialog.findViewById(R.id.et_fio);
        et_phone = (EditText)dialog.findViewById(R.id.et_phone);
        et_email = (EditText)dialog.findViewById(R.id.et_email);
        et_note = (EditText)dialog.findViewById(R.id.et_note);

        btn_export = (Button)dialog.findViewById(R.id.btn_export);
        btn_cancel = (Button)dialog.findViewById(R.id.btn_cancel);

        btn_cancel.setOnClickListener(cancel);
        btn_export.setOnClickListener(export);
        return dialog;
    }

    View.OnClickListener export = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utility.toXLS(
                    "file://" + Environment.getExternalStorageDirectory() + "/order.xls",
                    et_email.getText().toString(),
                    et_name.getText().toString(),
                    et_phone.getText().toString(),
                    et_note.getText().toString());
            File file = new File("file://" + Environment.getExternalStorageDirectory() + "/order.xls");
            if(!file.exists())
                Toast.makeText(getActivity().getApplicationContext(), "Не удалось создать отчет xls", Toast.LENGTH_LONG).show();

            onStop();
        }
    };

    View.OnClickListener cancel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onStop();
        }
    };
}
