package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sparrow on 17.10.15.
 */
public class Slide extends Enity{

    public enum Type
    {
        Group,
        Product
    }

    @SerializedName("_image")
    private String mImageUrl;

    @SerializedName("_group_id")
    private int mGroupId;

    @SerializedName("_product_id")
    private int mProductId;


    private Type mType;

    public Slide(int id, int revision, int imageUrlId, int groupId, int productId) {
        super(id, revision, imageUrlId);

        mGroupId = groupId;
        mProductId = productId;

        if(mGroupId != -1)
            mType = Type.Group;
        else if(mProductId != -1)
            mType = Type.Product;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public int getGroupId() {
        return mGroupId;
    }

    public void setGroupId(int groupId) {
        mGroupId = groupId;
    }

    public int getProductId() {
        return mProductId;
    }

    public void setProductId(int productId) {
        mProductId = productId;
    }

    public Type getType() {
        return mType;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues(super.getContentValue());

        contentValues.put("_group_id", getGroupId());
        contentValues.put("_product_id", getProductId());

        return contentValues;
    }


}
