package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sparrow on 17.10.15.
 */
public class Enity implements Serializable{

    @SerializedName("_id")
    private int mId;

    @SerializedName("_revision")
    private int mRevision;

    @SerializedName("_image_id")
    private int mImageUrlId;

    @SerializedName("_delete")
    private int mDelete;

    public Enity(int id, int revision, int imageUrlId) {
        mId = id;
        mRevision = revision;
        mImageUrlId = imageUrlId;
    }

    public int getId() {
        return mId;
    }

    public int getRevision() {
        return mRevision;
    }

    public int getImageUrlId() {
        return mImageUrlId;
    }

    public int getDelete() {
        return mDelete;
    }

    public void setDelete(int delete) {
        mDelete = delete;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("_id", mId);
        contentValues.put("_image_id", mImageUrlId);
        contentValues.put("_revision", mRevision);

        return contentValues;
    }
}
