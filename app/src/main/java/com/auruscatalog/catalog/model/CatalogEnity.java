package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sparrow on 21.10.15.
 */
public class CatalogEnity extends Enity {

    @SerializedName("_name")
    private String mName;

    public CatalogEnity(int id, int revision, int imageUrlId, String name) {
        super(id, revision, imageUrlId);

        mName = name;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    @Override
    public ContentValues getContentValue() {

        ContentValues contentValues = new ContentValues(super.getContentValue());

        contentValues.put("_name", mName);

        return contentValues;
    }
}
