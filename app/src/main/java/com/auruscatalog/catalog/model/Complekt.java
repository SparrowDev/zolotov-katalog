package com.auruscatalog.catalog.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sparrow on 17.10.15.
 */
public class Complekt extends CatalogEnity{

    @SerializedName("_image")
    private String mImageUrl;

    public Complekt(int id, int revision, int imageUrlId, String name) {
        super(id, revision, imageUrlId, name);
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }
}
