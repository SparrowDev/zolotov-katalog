package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sparrow on 17.10.15.
 */
public class Dictonary implements Serializable{

    @SerializedName("id")
    private int mId;

    @SerializedName("_name")
    private String mName;

    @SerializedName("_value")
    private String mValue;

    @SerializedName("_product_id")
    private int mProductId;

    @SerializedName("_revision")
    private int mRevision;

    public Dictonary(int id, String name, String value, int productId) {
        mId = id;
        mName = name;
        mValue = value;
        mProductId = productId;
    }

    public void setId(int id)
    {
        mId = id;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getValue() {
        return mValue;
    }

    public int getProductId() {
        return mProductId;
    }

    public ContentValues getContentParam()
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("_name", mName);

        return contentValues;
    }

    public void setRevision(int revision) {
        mRevision = revision;
    }

    public int getRevision() {
        return mRevision;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("_value", mValue);
        contentValues.put("_param_id", mId);
        contentValues.put("_product_id", mProductId);
        contentValues.put("_revision", mRevision);

        return contentValues;
    }
}
