package com.auruscatalog.catalog.model;

import android.content.ContentValues;
import android.text.format.DateUtils;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sparrow on 13.10.15.
 */
public class News extends Enity{

    @SerializedName("_title")
    private String mTitle;

    @SerializedName("_content")
    private String mContent;

    @SerializedName("_author")
    private String mAuthor;

    @SerializedName("_date")
    private String mDate;

    @SerializedName("_image")
    private String mImageUrl;

    public News(int id, int revision, int imageUrlId, String title, String content, String author, String date) {
        super(id, revision, imageUrlId);
        mTitle = title;
        mContent = content;
        mAuthor = author;
        mDate = date;
    }

    public void setTitle(String title) {
        if (title == null)
            throw new NullPointerException("Заголовок не может быть null");
        else
            mTitle = title;
    }

    public void setAuthor(String author) {
        if (author == null)
            throw new NullPointerException("Автор новости не может быть null");
        else
            mAuthor = author;
    }

    public void setContent(String content) {
        if (content == null)
            throw new NullPointerException("Содержание новости не может быть null");
        else
            mContent = content;
    }

    public void setDate(String date) {
        if (date == null)
            throw new NullPointerException("Дата не может быть null");
        else
            mDate = date;
    }


    public String getDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

        Date date = new Date();

        try {
            date = simpleDateFormat.parse(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return DateUtils.getRelativeTimeSpanString(date.getTime(), (new Date()).getTime(), 0).toString();
    }



    public String getTitle() {
        return mTitle;
    }

    public String getContent() {
        return mContent;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues(super.getContentValue());

        contentValues.put("_title", mTitle);
        contentValues.put("_content", mContent);
        contentValues.put("_author", mAuthor);
        contentValues.put("_date", mDate);

        contentValues.remove("_revision");

        return contentValues;
    }
}
