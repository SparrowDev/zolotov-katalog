package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sparrow on 17.10.15.
 */
public class Image implements Serializable{

    @SerializedName("_id")
    private int mId;

    @SerializedName("_image_url")
    private String mImageUrl;

    @SerializedName("_name")
    private String mName;

    @SerializedName("_revision")
    private int mRevision;

    @SerializedName("_delete")
    private int mDelete;

    @SerializedName("_weight")
    private int mWeight;

    @SerializedName("_height")
    private int mHeight;

    /**
     * @param imageUrl -url изображения
     * @param name - название изображения
     * @param revision - версия изображеняи
     */
    public Image(int id, String imageUrl, String name, int revision,  int weight, int height)
    {
        if(imageUrl == null)
            throw new NullPointerException("Ссылка на изображение не может быть null");
        mImageUrl = imageUrl;

        mId = id;

        mName = name;

        mRevision = revision;
        mWeight = weight;
        mHeight = height;
    }

    public int getId()
    {
        return mId;
    }

    public String getImageUrl()
    {
        return mImageUrl;
    }

    public String getName()
    {
        return mName == null ? "" : mName;
    }

    public int getRevision()
    {
        return mRevision;
    }

    public int getDelete() {
        return mDelete;
    }

    public void setDelete(int delete) {
        mDelete = delete;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("_id", getId());
        contentValues.put("_name", getName());
        contentValues.put("_image_url", getImageUrl());
        contentValues.put("_revision", getRevision());
        contentValues.put("_weight", getWeight());
        contentValues.put("_height", getHeight());

        return contentValues;
    }

    public int getWeight() {
        return mWeight;
    }

    public void setWeight(int weight) {
        mWeight = weight;
    }

    public int getHeight() {
        return mHeight;
    }

    public void setHeight(int height) {
        mHeight = height;
    }
}
