package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sparrow on 17.10.15.
 */
public class Product extends CatalogEnity implements Serializable{

    @SerializedName("_id_group")
    private int mIdGroup;

    @SerializedName("_articul")
    private String mArticul;

    @SerializedName("_image")
    private String mImageUrl;

    @SerializedName("_size")
    private Size[] mSizes;

    @SerializedName("_type_insert")
    private String mTypeInsert;

    @SerializedName("_type_product")
    private String mTypeProduct;

    @SerializedName("_material")
    private String mMaterial;

    @SerializedName("_type_insert_id")
    private int mTypeInsertId;

    @SerializedName("_type_product_id")
    private int mTypeProductId;

    @SerializedName("_material_id")
    private int mMaterialId;

    @SerializedName("_collection")
    private String mCollection;

    @SerializedName("_complekt_id")
    private int mComplektId;


    public Product(int id, int revision, int imageUrlId, int idGroup, String name, String articul, int typeInsertId, int typeProductId, int materialId, String collection, int complektId) {
        super(id, revision, imageUrlId, name);
        mIdGroup = idGroup;
        mArticul = articul;
        mTypeInsertId = typeInsertId;
        mTypeProductId = typeProductId;
        mMaterialId = materialId;
        mCollection = collection;
        mComplektId = complektId;
    }

    public int getIdGroup()
    {
        return mIdGroup;
    }


    public int getTypeInsertId()
    {
        return mTypeInsertId;
    }

    public int getTypeProductId()
    {
        return mTypeProductId;
    }

    public int getMaterialId()
    {
        return mMaterialId;
    }

    public int getComplektId()
    {
        return mComplektId;
    }

    public String getArticul() {
        return mArticul;
    }

    public String getCollection() {
        return mCollection;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getTypeInsert() {
        return mTypeInsert;
    }

    public String getTypeProduct() {
        return mTypeProduct;
    }

    public String getMaterial() {
        return mMaterial;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public void setTypeInsert(String typeInsert) {
        mTypeInsert = typeInsert;
    }

    public void setTypeProduct(String typeProduct) {
        mTypeProduct = typeProduct;
    }

    public void setMaterial(String material) {
        mMaterial = material;
    }

    public Size[] getSizes() {
        return mSizes;
    }

    public void setSizes(Size[] sizes) {
        mSizes = sizes;
    }

    @Override
    public ContentValues getContentValue() {
        ContentValues contentValues = new ContentValues(super.getContentValue());

        contentValues.put("_id_group", mIdGroup);
        contentValues.put("_articul", mArticul);
        contentValues.put("_type_insert_id", mTypeInsertId);
        contentValues.put("_type_product_id", mTypeProductId);
        contentValues.put("_material_id", mMaterialId);
        contentValues.put("_collection", mCollection);
        contentValues.put("_complekt_id", mComplektId);

        return contentValues;
    }
}
