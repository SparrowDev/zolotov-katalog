package com.auruscatalog.catalog.model;

import com.auruscatalog.catalog.database.DataBase;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sparrow on 17.10.15.
 */
public class DataController implements Serializable{

    private final String MATERIAL = "Material";

    private final String TYPE_INSERT = "TypeInsert";

    private final String TYPE_PRODUCT = "TypeProduct";

    private Group[] mGroups;

    private Product[] mProducts;

    private Image[] mImages;

    private News[] mNews;

    private Slide[] mSlides;

    private Size[] mSizes;

    private Dictonary[] mDictonary;

    private Complekt[] mComplekts;

    private DataBase mDataBase;

    private Basket mBasket;

    private static DataController mInstanse;

    public static DataController newInstanse(DataBase db)
    {
        if(mInstanse == null)
            mInstanse = new DataController(db);
        return mInstanse;
    }

    public DataController(DataBase db)
    {
        mDataBase = db;

        mImages = db.getImage();
        mSlides = db.getSlides();
        mComplekts = db.getComplekt();
        mNews = db.getNews();
        mProducts = db.getProduct();
        mGroups = db.getGroup();
        mSizes = db.getProductParametr();
        mDictonary = db.getDictonary();

        mBasket = Basket.getInstanse();
    }

    public void onCloseDB()
    {
        mDataBase = null;
    }

    public void onAttachDB(DataBase db)
    {
        if(mDataBase == null)
            mDataBase = db;
    }

    public Basket getBasket()
    {
        return mBasket;
    }

    public String getImageUrl(int id)
    {
        String result = "";

        if(mImages != null)
        {
            for(int i = 0; i<mImages.length; i++)
            {
                if(mImages[i] != null && mImages[i].getId() == id)
                {
                    result = mImages[i].getImageUrl();
                    break;
                }
            }
        }
        return result;
    }

    public int getGroupSize()
    {
        return mGroups != null ? mGroups.length : 0;
    }


    /**
     * Возвращает список групп входящих в указанную группу, для получения верхнего уровня списка -1
     * @param parent_id Идентификатор родительской группы
     * @return Список групп
     */
    public Group[] getGroupByParent(int parent_id)
    {
        if(mGroups != null)
        {
            ArrayList<Group> result = new ArrayList<>();

            for(int i = 0; i<mGroups.length; i++)
            {
                if(mGroups[i] != null && mGroups[i].getParentId() == parent_id)
                    result.add(mGroups[i]);
            }

            return result.toArray(new Group[0]);
        }
        else
            throw new NullPointerException("Список групп null");
    }

    public Group getGroup(int index)
    {
        if(mGroups != null && mGroups[index] != null)
        {
            Group result = mGroups[index];

            result.setImageUrl(getImageUrl(result.getImageUrlId()));

            return result;
        }
        else
            throw new NullPointerException("Группа по индексу " + index + " null");
    }

    /**
     * Возвращает индекс группы родителя по идентификатору группы
     * @param id Идентификатор группы
     * @return Индекс группы родителя
     */
    public int getGroupParentByGroupId(int id)
    {
        if(mGroups != null)
        {
            for(Group group : mGroups)
                if(group.getId() == id)
                    return group.getParentId();
            return -1;
        }
        else
            throw new NullPointerException("Массив групп null");
    }

    public Group[] getGroups() {
        return mGroups != null ? mGroups : new Group[0];
    }


    public void setGroups(Group[] groups) {
        if (groups == null)
            throw new NullPointerException("Список группн не может быть null");
        else
        {
            mDataBase.setGroup(groups);
            mGroups = mDataBase.getGroup();
        }
    }

    /**
     * Возвращает кол-во товаров в указанной группе (в сущности не нужен но пусть будет)
     * @param group_id - дентификатор группыы
     * @return Кол-во товара
     */
    public int getProductSize(int group_id)
    {
        int result = 0;
        if(mProducts != null)
        {
            for(int i  = 0; i<mProducts.length; i++)
            {
                if(mProducts[i] != null && mProducts[i].getIdGroup() == group_id)
                    result++;
            }
        }
        return result;
    }

    public Product getProduct(int product_id)
    {
        if(mProducts != null)
        {
            for(Product product : mProducts)
                if(product.getId() == product_id)
                    return product;
            return null;
        }
        else
            throw new NullPointerException("Массив продуктов null");
    }

    /**
     * Возвращает массив товаров в указанной группе
     * @param group_id - идентификатор группы
     * @return Массив товаров
     */
    public Product[] getProductByGroup(int group_id)
    {
        ArrayList<Product> result = new ArrayList<>();
        if(mProducts != null)
        {
            for(int i  = 0; i<mProducts.length; i++)
            {
                if(mProducts[i] != null && mProducts[i].getIdGroup() == group_id)
                {
                    result.add(getProduct(i, false));
                }
            }
        }
        else
            throw new NullPointerException("Массив продуктов null");

        return result.toArray(new Product[0]);
    }

    /**
     * Возвращает товар по указанному индексу
     * @param index - индекс товара
     * @param withDict Данные из словарей
     * @return Товар
     */
    private Product getProduct(int index, boolean withDict)
    {
        if(mProducts != null && mProducts[index] != null)
        {
            Product result = mProducts[index];

            Size[] param = getSize(mProducts[index].getId());

            // Задаем url изображения товара
            result.setImageUrl(getImageUrl(result.getImageUrlId()));
            // Задаем цену товара
            result.setSizes(param);

            return result;
        }
        else
            throw new NullPointerException("Продукт по индексу " + index + "null");
    }

    /**
     * Возвращае наибольшую ревизию продуктов
     * @return
     */
    public int getLastRevisionByProduct()
    {
        if(mProducts != null)
        {
            int result = 0;

            for(Product product : mProducts)
            {
                if(product.getRevision() > result)
                    result = product.getRevision();
            }

            return result;
        }
        else
            throw new NullPointerException("Массив продуктов null");
    }

    /**
     * Возвращает среднюю цену товара
     * @param product_id Идентификатор товара
     * @return Средняя цена товара
     */
    public int getAveragePriceProduct(int product_id)
    {
        Size[] arr = getSize(product_id);

        double min_price = Double.MAX_VALUE;
        double max_price = Double.MIN_VALUE;
        for (Size size : arr) {
            if (size.getPrice() > max_price)
                max_price = size.getPrice();
            if (size.getPrice() < min_price)
                min_price = size.getPrice();
        }

        int price = (int) ((min_price + max_price) / 2);

        return price;
    }

    /**
     * Возвращает цену комплекта
     * @param complekt_id Идентификатор комплекта
     * @return
     */
    public int getPriceComplekt(int complekt_id)
    {
        Product[] arr = getProductByComplekt(complekt_id);

        int min_price = Integer.MAX_VALUE;
        int max_price = Integer.MIN_VALUE;

        for(Product product : arr)
        {
            int temp = getAveragePriceProduct(product.getId());
            if(temp > max_price)
                max_price = temp;
            else if(temp < min_price)
                min_price = temp;
        }

        return (max_price+min_price)/2;
    }

    /**
     * Возвращает последнюю ревизию в массиве комплектов
     * @return
     */
    public int getLastRevisionByComplekt()
    {
        if(mComplekts != null)
        {
            int result = 0;

            for(Complekt complekt : mComplekts)
            {
                if(complekt.getRevision() > result)
                    result = complekt.getRevision();
            }

            return result;
        }
        else
            throw new NullPointerException("Список комплектов null");
    }

    /**
     * Возвращает параметры товара
     * @param product_id - индетификтаор товара
     * @return Параметры товара
     */
    public Size[] getSize(int product_id)
    {
        if(mSizes != null)
        {
            ArrayList<Size> result = new ArrayList<>();
            for(int i = 0; i<mSizes.length; i++)
                if(mSizes[i] != null && mSizes[i].getIdProduct() == product_id)
                {
                    result.add(mSizes[i]);
                }

            if(result != null)
                return result.toArray(new Size[0]);
            else
                throw new NullPointerException("Параметры для товара с индексом " + product_id + "не найдены");
        }
        else
            throw new NullPointerException("Параметры по индексу " + product_id + "null");
    }



    public Product[] getProducts() {
        return mProducts != null ? mProducts : new Product[0];
    }

    public void setProducts(Product[] products) {
        if(products == null)
            throw new NullPointerException("Список товаров не может быть null");
        else
        {
            mDataBase.setProduct(products);
            mProducts = mDataBase.getProduct();
        }
    }

    public Image[] getImages() {
        return mImages != null ? mImages : new Image[0];
    }

    public void setImages(Image[] images) {
        if(images == null)
            throw new NullPointerException("Список изображений не может быть null");
        else
        {
            mDataBase.setImage(images);
            mImages = mDataBase.getImage();
        }
    }

    public int getLastRevisionImage()
    {
        if(mImages != null)
        {
            int lastRevision = 0;
            for(Image image : mImages)
            {
                if(image.getRevision() > lastRevision)
                    lastRevision = image.getRevision();
            }

            return lastRevision;
        }
        else
            throw new NullPointerException("Массив изображений null");
    }


    /**
     * Возвращает новость по указанному индексу
     * @param index Индекс новости
     * @return Новость
     */
    private News getNewsByIndex(int index)
    {
        if(mNews != null)
        {
            News news = mNews[index];

            if(news == null)
                throw new NullPointerException("Новость по индексу " + index + " null");

            news.setImageUrl(getImageUrl(news.getImageUrlId()));

            return news;
        }
        else
            throw new NullPointerException("Список новостей null");
    }

    public int getLastRevisionByGroup()
    {
        if(mGroups != null)
        {
            int result = 0;

            for(Group group : mGroups)
            {
                if(group.getRevision() > result)
                    result = group.getRevision();
            }

            return result;
        }
        else
            throw new NullPointerException("Массив групп null");
    }

    public int getLastIdByNews()
    {
        if(mNews != null)
        {
            int result = 0;

            for(int i = 0; i<mNews.length; i++)
            {
                if(mNews[i].getId() > result)
                    result = mNews[i].getId();
            }

            return result;
        }
        else
            throw new NullPointerException("Массив новостей null");
    }

    /**
     * Возвращает массив новостей
     * @return
     */
    public News[] getNews() {

        if(mNews != null)
        {
            ArrayList<News> news = new ArrayList<>();

            for(int i = 0; i<mNews.length; i++)
            {
                news.add(getNewsByIndex(i));
            }

            return news.toArray(new News[0]);
        }
        else
            throw new NullPointerException("Массив новостей null");
    }

    public void setNews(News[] news) {
        if(news == null)
            throw new NullPointerException("Список новостей не может быть null");
        else
        {
            mDataBase.setNews(news);
            mNews = mDataBase.getNews();
        }
    }

    /**
     * Возвращает последнею ревизию слайдов
     * @return
     */
    public int getLastRevisionSlide()
    {
        if(mSlides != null)
        {
            int result = mSlides.length == 0 ? 0 : Integer.MIN_VALUE;

            for (int i = 0; i < mSlides.length; i++) {
                if(result < mSlides[i].getRevision())
                    result = mSlides[i].getRevision();
            }

            return result;
        }
        else
            throw new NullPointerException("Массив слайдов null");
    }

    public int getSlideCount()
    {
        if(mSlides != null)
            return mSlides.length;
        else
            throw new NullPointerException("Массив слайдов null");
    }

    /**
     * Возвращает массив слайдов
     * @return
     */
    public Slide[] getSlides() {
        if(mSlides != null)
        {
            ArrayList<Slide> slides = new ArrayList<>();

            for(int i = 0; i<mSlides.length; i++)
            {
                slides.add(getSlideByIndex(i));
            }

            return slides.toArray(new Slide[0]);
        }
        else
            throw new NullPointerException("Список слайдов null");
    }

    /**
     * Возвращает слайд по указанному индексу
     * @param index Индекс
     * @return Слайд
     */
    private Slide getSlideByIndex(int index)
    {
        if(mSlides != null)
        {
            Slide slide = mSlides[index];

            if(slide == null)
                throw new NullPointerException("Слайд по индексу " + index + " null");

            slide.setImageUrl(getImageUrl(slide.getImageUrlId()));

            return slide;
        }
        else
            throw new NullPointerException("Список слайдов null");
    }

    public void setSlides(Slide[] slides) {
        if(slides == null)
            throw new NullPointerException("Список слайдеров не может быть null");
        else
        {
            mDataBase.setSlides(slides);
            mSlides = mDataBase.getSlides();
        }
    }

    public Size[] getSizes() {
        return mSizes != null ? mSizes : new Size[0];
    }

    public void setSizes(Size[] sizes) {
        if(sizes == null)
            throw new NullPointerException("Список размеров не может быть null");
        else
        {
            mDataBase.setProductParametr(sizes);
            mSizes = mDataBase.getProductParametr();
        }
    }

    /**
     * Возвращает наибольшую ревизию параметров товаров
     * @return
     */
    public int getLastRevisionBySize()
    {
        if(mSizes != null)
        {
            int result = 0;

            for(Size size : mSizes)
                if(size.getRevision() > result)
                    result = size.getRevision();
            return result;
        }
        else
            throw new NullPointerException("Массив параметров null");
    }

    /**
     * Возвращает наибольшую ревизию параметров товара
     * @param product_id Идентификатор товара
     * @return
     */
    public int getLastRevisionBySize(int product_id)
    {
        if(mSizes != null)
        {
            int result = 0;

            for(Size size : mSizes)
                if(size.getIdProduct() == product_id && size.getRevision() > result)
                    result = size.getRevision();
            return result;
        }
        else
            throw new NullPointerException("Массив параметров null");
    }

    /**
     * Возвращает массив комплектов
     * @return
     */
    public Complekt[] getComplekts() {
        if(mComplekts != null)
        {
            ArrayList<Complekt> complekts = new ArrayList<>();

            for(int i  = 0; i<mComplekts.length; i++)
            {
                complekts.add(getComplektByIndex(i));
            }

            return complekts.toArray(new Complekt[0]);
        }
        else
            throw new NullPointerException("Массив комплектов null");
    }

    /**
     * Возвращает комплект по указанному индексу
     * @param index Индекс
     * @return Комплект
     */
    private Complekt getComplektByIndex(int index)
    {
        if(mComplekts != null)
        {
            Complekt complekt = mComplekts[index];

            if(complekt == null)
                throw new NullPointerException("Комплект по индексу " + index + " null");

            complekt.setImageUrl(getImageUrl(complekt.getImageUrlId()));

            return complekt;
        }
        else
            throw new NullPointerException("Массив комплектов null");
    }

    /**
     * Массив товаров из указанного комплекта
     * @param complekt_id Идентификатор комплекта
     * @return Массив товаров
     */
    public Product[] getProductByComplekt(int complekt_id)
    {
        if(mProducts != null)
        {
            ArrayList<Product> result = new ArrayList<>();

            for(int i = 0; i<mProducts.length; i++)
            {
                if(mProducts[i] != null && mProducts[i].getComplektId() == complekt_id)
                {
                    result.add(getProduct(i, false));
                }
            }

            return result.toArray(new Product[0]);
        }
        else
            throw new NullPointerException("Массив продуктов null");
    }

    public void setComplekts(Complekt[] complekts) {
        if(complekts == null)
            throw new NullPointerException("Список комплектов не может быть null");
        else
        {
            mDataBase.setComplekt(complekts);
            mComplekts = mDataBase.getComplekt();
        }
    }

    /**
     * Возвращает массив параметров указанного товара
     * @param id Идентификатор товара
     * @return
     */
    public Dictonary[] getDictonaryByProduct(int id)
    {
        if(mDictonary != null)
        {
            ArrayList<Dictonary> dictonaries = new ArrayList<>();

            for(Dictonary dictonary : mDictonary)
            {
                if(dictonary.getProductId() == id)
                    dictonaries.add(dictonary);
            }

            return dictonaries.toArray(new Dictonary[0]);
        }
        else
            throw new NullPointerException("Массив параметров null");
    }

    public void setDictonary(Dictonary[] dictonary)
    {
        if(dictonary != null)
        {
            mDataBase.setDictonary(dictonary);
            mDictonary = mDataBase.getDictonary();
        }
        else
            throw new NullPointerException("Массив параметров не может быть null");
    }

    public int getLastRevisionByDictonary(int product_id)
    {
        if(mDictonary != null)
        {
            int last = -1;
            for(Dictonary dictonary : mDictonary)
            {
                if(dictonary.getProductId() == product_id && dictonary.getRevision() > last)
                    last = dictonary.getRevision();
            }
            return last;
        }
        else
            throw new NullPointerException("Массив словарей null");
    }

}
