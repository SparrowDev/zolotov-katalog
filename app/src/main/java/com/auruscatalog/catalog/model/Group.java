package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sparrow on 17.10.15.
 */
public class Group extends CatalogEnity{

    @SerializedName("_parent_id")
    private int mParentId;

    @SerializedName("_image")
    private String mImageUrl;

    public Group(int id, int revision, int imageUrlId, int parentId, String name) {
        super(id, revision, imageUrlId, name);
        mParentId = parentId;
    }

    public int getParentId() {
        return mParentId;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues(super.getContentValue());

        contentValues.put("_parent_id", mParentId);

        return contentValues;
    }
}
