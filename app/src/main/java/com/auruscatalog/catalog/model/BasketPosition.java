package com.auruscatalog.catalog.model;


import java.io.Serializable;

/**
 * Created by sparrow on 06.11.15.
 */
public class BasketPosition implements Serializable{

    Product mProduct;

    Size mSize;

    int mCount;

    public BasketPosition(Product product, Size size, int count)
    {
        mProduct = product;
        mSize = size;

        if(count >= 0)
            mCount = count;
        else
            mCount = 0;
    }

    public String getImageUrl()
    {
        if(mProduct != null)
            return mProduct.getImageUrl();
        else
            return "";
    }

    public String getProductName()
    {
        if(mProduct != null)
            return mProduct.getName();
        return "";
    }

    public double getPrice()
    {
        if(mSize != null)
        {
            return  mSize.getPrice() * mCount;
        }
        else
            return 0;
    }

    public Size getSize() {
        return mSize;
    }

    public int getCount() {
        return mCount;
    }

    public void setCount(int count) {
        mCount = count;
    }
}
