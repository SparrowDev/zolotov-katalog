package com.auruscatalog.catalog.model;

import android.content.ContentValues;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by sparrow on 17.10.15.
 */
public class Size implements Serializable {

    @SerializedName("_id")
    private int mId;

    @SerializedName("_id_product")
    private int mIdProduct;

    @SerializedName("_size")
    private double mSize;

    @SerializedName("_weight")
    private double mWeight;

    @SerializedName("_price")
    private double mPrice;

    @SerializedName("_revision")
    private int mRevision;

    @SerializedName("_delete")
    private int mDelete;

    public Size(int id, int idProduct, double size, double weight, double price, int revision) {
        mId = id;
        mIdProduct = idProduct;
        mSize = size;
        mWeight = weight;
        mPrice = price;
        mRevision = revision;
    }

    public int getId() {
        return mId;
    }

    public int getIdProduct() {
        return mIdProduct;
    }

    public double getSize() {
        return mSize;
    }

    public double getWeight() {
        return mWeight;
    }

    public double getPrice() {
        return mPrice;
    }

    public int getRevision() {
        return mRevision;
    }

    public int getDelete() {
        return mDelete;
    }

    public void setDelete(int delete) {
        mDelete = delete;
    }

    public ContentValues getContentValue()
    {
        ContentValues contentValues = new ContentValues();

        contentValues.put("_id", mId);
        contentValues.put("_id_product", mIdProduct);
        contentValues.put("_size", mSize);
        contentValues.put("_weight", mWeight);
        contentValues.put("_price", mPrice);
        contentValues.put("_revision", mRevision);

        return contentValues;
    }
}
