package com.auruscatalog.catalog.model;

import com.auruscatalog.catalog.utility.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by sparrow on 06.11.15.
 */
public class Basket implements Serializable{

    public class UpdatePosition
    {
        int mUpdate;

        int mNews;

        double mShiftPrice;

        public UpdatePosition()
        {
            mNews = 0;
            mUpdate = 0;
            mShiftPrice = 0;
        }

        public UpdatePosition(int update, int news, double shiftPrice)
        {
            mUpdate = update > -1 ? update : 0;
            mNews = news > -1 ? news : 0;

            mShiftPrice = shiftPrice >= 0 ? shiftPrice : 0;
        }

        public void setUpdate(int update) {
            mUpdate = update;
        }

        public void setNews(int news) {
            mNews = news;
        }

        public void setShiftPrice(double shiftPrice) {
            mShiftPrice = shiftPrice;
        }

        public int getUpdate()
        {
            return mUpdate;
        }

        public int getNews()
        {
            return mNews;
        }

        public double getShiftPrice()
        {
            return mShiftPrice;
        }

        public String getMessage()
        {
            String message = "";
            if(getUpdate() > 0 || getNews() > 0) {
                message = "В корзину добавлено: " + getNews() + " " +
                        Utility.getCorrectDeclinationWordsGoods(getNews()) + ", обновлено: " +
                        "" + getUpdate() + " " + Utility.getCorrectDeclinationWordsGoods(getUpdate()) +
                        " общая сумма заказа изменилась на " + getShiftPrice();
            }
            else
                message = "Нечего не было обновлено";
            return message;
        }
    }

    ArrayList<BasketPosition> mPositions = new ArrayList<>();

    static Basket mInstanse;

    public static Basket getInstanse()
    {
        if(mInstanse == null)
            mInstanse = new Basket();
        return mInstanse;
    }

    public void onAddPosition(BasketPosition position)
    {
        if(position != null)
            mPositions.add(position);
        else
            throw new NullPointerException("Позиция товара не может быть null");
    }

    public BasketPosition onRemovePositionByIndex(int index)
    {
        BasketPosition basketPosition = mPositions.get(index);
        mPositions.remove(index);
        return basketPosition;
    }

    public BasketPosition getPosition(int index)
    {
        if(mPositions != null)
            return mPositions.get(index);
        else
            throw new NullPointerException("Список позиций в корзине null");
    }

    /**
     * Добавляет и/или обновляет позиции товара в корзину(е)
     * @param positions Позиции товара для добавления
     * @return Кол-во новых позиций и кол-во обновленных
     */
    public UpdatePosition onAddPosition(BasketPosition[] positions)
    {
        UpdatePosition updatePosition = new UpdatePosition();
        if(positions != null)
        {
            //Пробегаем по всем новым позициям товаров
            for(BasketPosition position : positions)
            {
                boolean first = true;

                //Пробегаем по товаром в корзине
                for(BasketPosition pos : mPositions)
                {
                    if(!(first = !(pos.getSize() == position.getSize()) && pos.getCount() != position.getCount()))
                    {
                        updatePosition.setShiftPrice(updatePosition.getShiftPrice() + (position.getPrice() - pos.getPrice()));
                        updatePosition.setUpdate(updatePosition.getUpdate() + position.getCount() - pos.getCount());
                        pos.setCount(position.getCount());
                        first = false;
                    }
                }

                if(first)
                {
                    mPositions.add(position);
                    updatePosition.setShiftPrice(updatePosition.getShiftPrice() + position.getPrice());
                    updatePosition.setNews(updatePosition.getNews() + position.getCount());
                }
            }
        }

        return updatePosition;
    }

    public BasketPosition[] getPositionByProduct(Product product)
    {
        if(mPositions == null)
            return null;

        List<BasketPosition> result = new ArrayList<>();

        for(BasketPosition position : mPositions)
        {
            if(position.mProduct.getId() ==  product.getId())
                result.add(position);
        }
        return result.toArray(new BasketPosition[result.size()]);
    }

    /**
     * Изменяет значение указанной позиции товара на 1 (+ или -)
     * @param index Индекс того товара количество которого надо поменять
     * @param diff Увеличить или уменьшить кол-во товара по индексу, true - увеличить, false - уменьшить
     */
    public void onAddToPosition(int index, boolean diff)
    {
        if(index < mPositions.size() && mPositions.get(index) != null)
            mPositions.get(index).mCount = diff ? mPositions.get(index).mCount + 1 : mPositions.get(index).mCount - 1;
    }

    public int getCount()
    {
        if(mPositions != null)
            return mPositions.size();
        else
            return 0;
    }

    public double getPrice()
    {
        double result = 0;
        if(mPositions == null)
            return result;

        for(BasketPosition position : mPositions)
        {
            result += position.getPrice();
        }
        return result;
    }

}
