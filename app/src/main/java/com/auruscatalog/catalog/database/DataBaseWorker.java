package com.auruscatalog.catalog.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.io.Serializable;

/**
 * Created by Воробей on 20.10.2014.
 */
public class DataBaseWorker extends SQLiteOpenHelper implements BaseColumns, Serializable{

	private static final int db_version = 1;


	private static final String dbPath = "data/data/com.auruscatalog.catalog/databases/catalog.db";

	public DataBaseWorker(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	public DataBaseWorker(Context context)
	{
		super(context, "catalog.db", null, db_version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
}

