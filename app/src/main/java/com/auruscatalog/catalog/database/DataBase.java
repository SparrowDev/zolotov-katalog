package com.auruscatalog.catalog.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import com.auruscatalog.catalog.R;
import com.auruscatalog.catalog.model.Complekt;
import com.auruscatalog.catalog.model.Dictonary;
import com.auruscatalog.catalog.model.Group;
import com.auruscatalog.catalog.model.Image;
import com.auruscatalog.catalog.model.News;
import com.auruscatalog.catalog.model.Size;
import com.auruscatalog.catalog.model.Slide;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Воробей on 23.10.2014.
 */
public class DataBase implements Serializable{

    public final String TABLE_IMAGE = "Images";

    public final String TABLE_SLIDES = "Slides";

    public final String TABLE_PRODUCT = "Product";

    public final String TABLE_SIZE = "Product_parametr";

    public final String TABLE_GROUP = "Group";

    public final String TABLE_COMPLEKT = "Complekt";

    public final String TABLE_INFORMATION = "Information";

    public final String TABLE_MATERIAL = "Material";

    public final String TABLE_INSERT_TYPE = "Insert_type";

    public final String TABLE_TYPE_PRODUCT = "Type_product";


	/* Сущность БД */
	private SQLiteDatabase sdb;
	/* Класс для работы с БД */
	private DataBaseWorker dbw;

    private final String dbName = "catalog";
    private final String dbPath = "data/data/com.auruscatalog.catalog/databases/";

    private final Context mContext;

	/* Context - это окружение из главнного класса приложения пишем this */
	public DataBase(Context context) {
		dbw = new DataBaseWorker(context);
        mContext = context;

        OpenDataBase();
	}

    /**
     * Открывает Базу данных на чтение
     * @throws SQLiteException
     */
    private void OpenDataBase() throws SQLiteException
    {
        try
        {
            onCreateDataBase();
        }
        catch (IOException e)
        {

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        String toPath = this.dbPath + this.dbName;

      //  sdb = dbw.getWritableDatabase();

        sdb = SQLiteDatabase.openDatabase(toPath, null, SQLiteDatabase.OPEN_READWRITE);

    }

    /**
     * Проверяет существует ли База Данных
     * @ Возвращает true - если существует, false - если не существует
     */
    private boolean isCheckDataBase()
    {
        SQLiteDatabase check = null;

        String path = this.dbPath + this.dbName;

        try {
            check = SQLiteDatabase.openDatabase(path, null,SQLiteDatabase.OPEN_READONLY);
        }
        catch (SQLiteException e)
        {
            check = null;
        }

        if(check != null)
        {
            check.close();
        }

        return check != null ? true : false;
    }

    /**
     * Создает Базу Данных, если она еще не существует
      * @throws IOException
     */
    private void onCreateDataBase() throws IOException, NoSuchAlgorithmException {
        boolean dbExist = isCheckDataBase();

        if(!dbExist)
        {
            this.dbw.getReadableDatabase();

            try {
                onCopyDataBase();
            }
            catch (SQLiteException e)
            {
                throw new Error("Error copy Data base");
            }
        }
        else {
           /* MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            MessageDigest messageDigest1 = MessageDigest.getInstance("MD5");


            InputStream inputStream = new FileInputStream(this.dbPath + this.dbName);
            inputStream = new DigestInputStream(inputStream, messageDigest);

            InputStream inputStream1 = this.mContext.getResources().openRawResource(R.raw.catalog);
            inputStream1 = new DigestInputStream(inputStream1, messageDigest1);

            byte[] bytes = messageDigest.digest();
            byte[] bytes1 = messageDigest1.digest();

            if (!bytes.equals(bytes1)) {
                File f = new File(this.dbPath + this.dbName);
                if (f.exists()) {
                    f.delete();
                    this.dbw.getReadableDatabase();

                    try {
                        onCopyDataBase();
                    } catch (SQLiteException e) {
                        throw new Error("Error copy Data base");
                    }
                }
            }*/
        }
    }

    public void onClose()
    {
        if(sdb != null)
        {
            sdb.close();
            sdb = null;
        }

    }

    public void onStart()
    {
        OpenDataBase();
    }

    /**
     * Копирует База Данных из папки с ресурсами в папку для работы с БД
     * @throws IOException
     */
    private void onCopyDataBase() throws IOException
    {
        InputStream inputStream = this.mContext.getResources().openRawResource(R.raw.catalog);

        String toPath = this.dbPath + this.dbName;


        OutputStream outputStream = new FileOutputStream(toPath);

        byte[] buffer = new byte[1024];
        int length;

        while ((length = inputStream.read(buffer)) > 0)
        {
            outputStream.write(buffer, 0 , length);
        }

        outputStream.flush();
        outputStream.close();
        inputStream.close();
    }


    /**
     * Заносит список параметров в базу данных
     * @param dictonary Массив параметров
     */
    public void setDictonary(Dictonary[] dictonary)
    {
        for(int i = 0; i<dictonary.length; i++) {
            sdb.insert("ProductParametrs", null, dictonary[i].getContentParam());

            Cursor cursor = sdb.query(
                    "ProductParametrs",
                    new String[] {"_id"},
                    null,
                    null,
                    null,
                    null,
                    null);

            cursor.moveToLast();
            dictonary[i].setId(cursor.getInt(cursor.getColumnIndex("_id")));

            sdb.insert("ProductParametrs_Value", null, dictonary[i].getContentValue());
        }
    }

    /**
     * Возвращает массив параметров
     * @return
     */
    public Dictonary[] getDictonary()
    {
        Cursor cursor = sdb.query("ProductParametrs", new String[] { "_id", "_name" }, null, null, null, null, null);

        if(cursor.getCount() != 0)
        {
            Cursor cursor1 = sdb.query("ProductParametrs_Value", new String[] { "_value", "_param_id", "_revision", "_product_id" }, null, null, null, null, null);

            Dictonary[] result = new Dictonary[cursor.getCount()];

            int i = 0;
            while (cursor.moveToNext())
            {
                cursor1.moveToNext();
                int id = cursor.getInt(cursor.getColumnIndex("_id"));

                String value = "";
                int product_id = -1;
                int revision = -1;


                for(int j = 0; j<cursor1.getCount(); j++) {
                    cursor1.moveToPosition(j);

                    if (cursor1.getInt(cursor1.getColumnIndex("_param_id")) == id) {
                        value = cursor1.getString(cursor1.getColumnIndex("_value"));
                        product_id = cursor1.getInt(cursor1.getColumnIndex("_product_id"));
                        revision = cursor1.getInt(cursor1.getColumnIndex("_revision"));
                        break;
                    }
                }


                result[i] = new Dictonary
                        (
                                id,
                                cursor.getString(cursor.getColumnIndex("_name")),
                                value,
                                product_id
                        );

                result[i].setRevision(revision);
                i++;
            }

            return result;
        }
        else
            return new Dictonary[0];
    }

    /**
     * Вносит массив комплектов в базу данных
     * @param complekt
     */
    public void setComplekt(Complekt[] complekt)
    {
        for (Complekt aComplekt : complekt) {
            if (aComplekt.getDelete() == 1)
                sdb.execSQL("DELETE FROM Complekt WHERE _id = " + aComplekt.getId());
        }

        for (Complekt aComplekt : complekt) {
            sdb.insert("Complekt", null, aComplekt.getContentValue());
        }
    }

    /**
     * Возвращает массив комлпектов
     * @return
     */
    public Complekt[] getComplekt()
    {
        Cursor cursor = sdb.query("Complekt", new String[] {"_id", "_name", "_image_id", "_revision"}, null, null, null, null, null);

        Complekt[] result = new Complekt[cursor.getCount()];

        int i = 0;
        while (cursor.moveToNext())
        {
            result[i] = new Complekt
                    (
                            cursor.getInt(cursor.getColumnIndex("_id")),
                            cursor.getInt(cursor.getColumnIndex("_revision")),
                            cursor.getInt(cursor.getColumnIndex("_image_id")),
                            cursor.getString(cursor.getColumnIndex("_name"))
                    );
            i++;
        }

        cursor.close();
        return result;
    }

    /**
     * Добавляет массив параметры товара
     * @param size
     */
    public void setProductParametr(Size[] size)
    {
        for (Size aSize : size) {
            if (aSize.getDelete() == 1)
                sdb.execSQL("DELETE FROM Product_parametr WHERE _id = " + aSize.getId());
        }

        for (Size aSize : size) {
            sdb.insert("Product_parametr", null, aSize.getContentValue());
        }
    }

    /**
     * Возвращает массив параметров
     * @return
     */
    public Size[] getProductParametr()
    {
        Cursor cursor = sdb.query("Product_parametr", new String[] { "_id", "_id_product", "_size", "_weight", "_price", "_revision"}, null, null, null, null, null);

        Size[] result = new Size[cursor.getCount()];

        int i = 0;
        while (cursor.moveToNext())
        {
            result[i] = new Size
                    (
                            cursor.getInt(cursor.getColumnIndex("_id")),
                            cursor.getInt(cursor.getColumnIndex("_id_product")),
                            cursor.getDouble(cursor.getColumnIndex("_size")),
                            cursor.getDouble(cursor.getColumnIndex("_weight")),
                            cursor.getDouble(cursor.getColumnIndex("_price")),
                            cursor.getInt(cursor.getColumnIndex("_revision"))
                    );
            i++;
        }
        cursor.close();
        return result;
    }

    /**
     * Возвращает массив товаров
     * @return
     */
    public com.auruscatalog.catalog.model.Product[] getProduct()
    {
        Cursor cursor = sdb.query(
                "Product",
                new String[]
                        {
                                "_id",
                                "_id_group",
                                "_image_id",
                                "_name",
                                "_articul",
                                "_type_insert_id",
                                "_type_product_id",
                                "_material_id",
                                "_collection",
                                "_complekt_id",
                                "_revision"
                        },
                null,
                null,
                null,
                null,
                null);

        com.auruscatalog.catalog.model.Product[] result = new com.auruscatalog.catalog.model.Product[cursor.getCount()];

        int i = 0;
        while (cursor.moveToNext())
        {
            result[i] = new com.auruscatalog.catalog.model.Product(
                    cursor.getInt(cursor.getColumnIndex("_id")),
                    cursor.getInt(cursor.getColumnIndex("_revision")),
                    cursor.getInt(cursor.getColumnIndex("_image_id")),
                    cursor.getInt(cursor.getColumnIndex("_id_group")),
                    cursor.getString(cursor.getColumnIndex("_name")),
                    cursor.getString(cursor.getColumnIndex("_articul")),
                    cursor.getInt(cursor.getColumnIndex("_type_insert_id")),
                    cursor.getInt(cursor.getColumnIndex("_type_product_id")),
                    cursor.getInt(cursor.getColumnIndex("_material_id")),
                    cursor.getString(cursor.getColumnIndex("_collection")),
                    cursor.getInt(cursor.getColumnIndex("_complekt_id"))
            );
            i++;
        }

        cursor.close();
        return result;
    }

    /**
     * Возвращает список слайдов
     * @return
     */
    public Slide[] getSlides()
    {
        Cursor cursor = sdb.query("Slides", new String[] {"_id", "_image_id", "_revision", "_group_id", "_product_id"}, null, null, null, null, null);

        Slide[] slides = new Slide[cursor.getCount()];

        int i = 0;

        while (cursor.moveToNext())
        {
            slides[i] = new Slide(
                    cursor.getInt(cursor.getColumnIndex("_id")),
                    cursor.getInt(cursor.getColumnIndex("_revision")),
                    cursor.getInt(cursor.getColumnIndex("_image_id")),
                    cursor.getInt(cursor.getColumnIndex("_group_id")),
                    cursor.getInt(cursor.getColumnIndex("_product_id"))
            );
            i++;
        }
        cursor.close();
        return slides;
    }

    /**
     * Добавляет массив слайдов в БД
     * @param slides Массив слайдеров для добавления/обновления в БД
     */
    public void setSlides(Slide[] slides)
    {
        for (Slide slide : slides) {
            if (slide.getDelete() == 1)
                sdb.execSQL("DELETE FROM Slides WHERE _id = " + slide.getId());
        }

        for (Slide slide : slides) {
            sdb.insert("Slides", null, slide.getContentValue());
        }
    }

    /**
     * Возвращает последнюю ревизию из указанной таблицы
     * @return
     */
    public int getLastRevision(String table)
    {
        Cursor cursor = sdb.query(table, new String[] {"max(_revision) as max"}, null, null, null, null, null);

        int result = 0;
        if(cursor.getCount() > 0)
        {
            cursor.moveToNext();
            result = cursor.getInt(cursor.getColumnIndex("max"));

            cursor.close();
            return result;
        }
        else
            return result;
    }

    /**
     * Добавляет в базу масив товаров
     * @param product Массив продуктов для добавления/обновления в БД
     */
    public void setProduct(com.auruscatalog.catalog.model.Product[] product)
    {

        /**
         * Удаляем позиции помеченные сервером как удаленные
         */
        for (com.auruscatalog.catalog.model.Product aProduct : product) {
            if (aProduct.getDelete() == 1) {
                sdb.execSQL("DELETE FROM PRODUCT WHERE _id = " + aProduct.getId());
            }
        }

        for (com.auruscatalog.catalog.model.Product aProduct : product) {
            sdb.insert("Product", null, aProduct.getContentValue());
        }
    }

    /**
     * Возвращает массив изображений
     * @return Массив изображений
     */
    public Image[] getImage()
    {
        Cursor cursor = sdb.query(
                "Image",
                new String[] { "_id", "_image_url", "_name", "_revision", "_weight", "_height"},
                null,
                null,
                null,
                null,
                null);

        Image[] result = new Image[cursor.getCount()];

        int i = 0;
        while (cursor.moveToNext())
        {
            result[i] = new Image(
                    cursor.getInt(cursor.getColumnIndex("_id")),
                    cursor.getString(cursor.getColumnIndex("_image_url")),
                    cursor.getString(cursor.getColumnIndex("_name")),
                    cursor.getInt(cursor.getColumnIndex("_revision")),
                    cursor.getInt(cursor.getColumnIndex("_weight")),
                    cursor.getInt(cursor.getColumnIndex("_height"))
            );
            i++;
        }
        cursor.close();
        return result;
    }

    /**
     * Заносит массив изображений в базу данных
     * @param image Массив изображения для добавления/обновления в БД
     */
    public void setImage(Image[] image)
    {
        for (Image anImage : image) {
            if (anImage.getDelete() == 1)
                sdb.execSQL("DELETE FROM Images WHERE _id = " + anImage.getId());
        }

        for (Image anImage : image) {
            sdb.insert("Image", null, anImage.getContentValue());
        }
    }

    /**
     * Возвращает массив новостей
     * @return Массив новостей из БД
     */
    public News[] getNews()
    {
        Cursor cursor = sdb.query(
                "News",
                new String[] { "_id", "_title", "_content", "_author", "_date", "_image_id"},
                null,
                null,
                null,
                null,
                null);

        News[] result = new News[cursor.getCount()];


        if(cursor.getCount() != 0) {
            int i = 0;
            while (cursor.moveToNext()){

               // SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                try {
                    result[i] = new News(
                            cursor.getInt(cursor.getColumnIndex("_id")),
                            0,
                            cursor.getInt(cursor.getColumnIndex("_image_id")),
                            cursor.getString(cursor.getColumnIndex("_title")),
                            cursor.getString(cursor.getColumnIndex("_content")),
                            cursor.getString(cursor.getColumnIndex("_author")),
                            cursor.getString(cursor.getColumnIndex("_date"))
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                i++;
            }
        }
        cursor.close();
        return result;
    }

    /**
     * Вносит в базу массив новостей
     * @param news - массив новостей
     */
    public void setNews(News[] news)
    {
        for (News aNew : news) {
            if (aNew.getDelete() == 1)
                sdb.execSQL("DELETE FROM News WHERE _id = " + aNew.getId());
        }

        for (News aNew : news) {
            sdb.insert("News", null, aNew.getContentValue());
        }
    }

    /**
     * Возвращает массив групп
     * @return Массив групп
     */
    public Group[] getGroup()
    {
        Cursor cursor = sdb.query("Groups", new String[] {"_id", "_parent_id", "_image_id", "_name", "_revision"}, null, null, null, null, null);

        Group[] result = new Group[cursor.getCount()];

        int i = 0;
        while (cursor.moveToNext())
        {
            result[i] = new Group
                    (
                            cursor.getInt(cursor.getColumnIndex("_id")),
                            cursor.getInt(cursor.getColumnIndex("_revision")),
                            cursor.getInt(cursor.getColumnIndex("_image_id")),
                            cursor.getInt(cursor.getColumnIndex("_parent_id")),
                            cursor.getString(cursor.getColumnIndex("_name"))
                    );
            i++;
        }
        cursor.close();
        return result;
    }


    /**
     * Заносит массив групп в базу данных
     * В том числе производит удаление и обновление существующих записей
     * @param groups массив групп
     */
    public void setGroup(Group[] groups)
    {
        for (Group group : groups) {
            if (group != null && group.getDelete() == 1)
                sdb.execSQL("DELETE FROM Group WHERE _id = " + group.getId());
        }

        for (Group group : groups) {
            sdb.insert("Groups", null, group.getContentValue());
        }
    }



    /**
     * Возвращает id последней новости
     * @return Последний id новости
     */
    public int getLastIdNews()
    {
        Cursor cursor = sdb.query("News", new String[] { "max(_id) as max"}, null, null, null, null, null);

        int result = -1;

        while (cursor.moveToNext()) {
            result = cursor.getInt(cursor.getColumnIndex("max"));
        }
        cursor.close();
        return result;
    }
}