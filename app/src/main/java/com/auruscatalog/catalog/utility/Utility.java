package com.auruscatalog.catalog.utility;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.View;

import com.auruscatalog.catalog.model.Basket;
import com.auruscatalog.catalog.model.BasketPosition;

import jxl.CellFeatures;
import jxl.CellType;
import jxl.LabelCell;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.write.DateTime;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class Utility
{

    private static WritableCellFormat timesBoldUnderline;
    private static WritableCellFormat times;

    /**
	 * Разделяет входящую строку символом \n для получения столбика строк указанной длины
	 * @param text входящая строка
	 * @param size размер подстроки
	 * @return форматированная строка
	 */
	public static String getDivideString(String text, int size)
	{
		if(text.length() > size * 2 - 2)
		{
			text = text.substring(0, size * 2 - 2) + "...";
		}
		
		if(text.length() > size)
		{
			int i = size;
			for(; text.charAt(i) != ' ' ; i--);

			text = new StringBuffer(text).insert(i, "\n").toString();
		}
	
		return text;
	}
	
	/**
	 * Преобразует строку вида 10000 в строку вида 10 000
	 * @param price цена
	 * @return преобразованая цена
	 */
	public static String reformatPrice(Double price)
	{
		String p = Double.toString(price);
		StringBuilder stringBuilder = new StringBuilder(p);
		for(int i = p.length(); i > 0; i-= i == p.length() ? 5 : 3)
			stringBuilder.insert(i, ' ');
		
		return stringBuilder.toString() + "ք";
	}
	
	public static int dpToPx(Resources res, int dp)
	{
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, res.getDisplayMetrics());
	}
	
	public static String getDateString()
	{
		DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
		df.setLenient(true);
		Date date = Calendar.getInstance().getTime();
		String formattedDate = df.format(date);
		return formattedDate;
	}

    /**
     * Метод возвращающий правильное склонение слова товар
     * @param count кол-во товаров
     * @return Слово в верном склонении
     */
    public static String getCorrectDeclinationWordsGoods(int count)
    {
        if(count < 0)
            count = Math.abs(count);
        if(count > 10 && count < 21)
            return "товаров";
        switch (count % 10)
        {
            case 1:
                return "товар";
            case 2:
            case 3:
            case 4:
                return "товара";
            default:
                return "товаров";
        }
    }

	public static void crossfade(final View animationView, int Duration) {

        /*
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        mContentView.setAlpha(0f);
        mContentView.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        mContentView.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);*/

		// Animate the loading view to 0% opacity. After the animation ends,
		// set its visibility to GONE as an optimization step (it won't
		// participate in layout passes, etc.)
		animationView.animate()
				.alpha(0f)
				.setDuration(Duration)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						animationView.setVisibility(View.GONE);
					}
				});
	}

    public static void toXLS(String path, String email, String name, String phone, String note)
    {
        File file = new File(path);
        //file.delete();

        Basket basket = Basket.getInstanse();

        WorkbookSettings wb_settings = new WorkbookSettings();
        wb_settings.setLocale(new Locale("ru", "RU"));

        try
        {
            WritableWorkbook workbook = Workbook.createWorkbook(file, wb_settings);
            workbook.createSheet("Заказ", 0);
            WritableSheet sheet = workbook.getSheet(0);
            sheet.setColumnView(0, 8);
            sheet.setColumnView(1, 19);
            sheet.setColumnView(2, 19);
            sheet.setColumnView(3, 7);
            sheet.setColumnView(4, 12);
            sheet.setColumnView(5, 8);
            sheet.setColumnView(6, 12);

            WritableCellFormat f_center = new WritableCellFormat();
            f_center.setAlignment(Alignment.CENTRE);

            Label org_name = new Label(0, 0, "ООО \"Золотов\"");
            Label org_email = new Label(4, 0, "zolotov@zolotov.ru");
            Label org_contact = new Label(0, 1, "Контактный телефон: 8 (495) 785-66-55");
            Label org_order = new Label(0, 2, "Заказ", f_center);
            Label org_client = new Label(0, 3, "Заказчик: " + name);
            Label org_date = new Label(0, 4, "Дата заказа: " + Utility.getDateString());
            Label org_client_email = new Label(0, 5, "Электронный адрес: " + email);
            Label org_client_phone = new Label(0, 6, "Телефон: " + phone);
            Label org_note = new Label(0, 7, "Комментарий: " + note);

            sheet.mergeCells(0, 0, 3, 0);
            sheet.mergeCells(4, 0, 6, 0);
            sheet.mergeCells(0, 1, 6, 1);
            sheet.mergeCells(0, 2, 6, 2);
            sheet.mergeCells(0, 3, 6, 3);
            sheet.mergeCells(0, 4, 6, 4);
            sheet.mergeCells(0, 5, 6, 5);
            sheet.mergeCells(0, 6, 6, 6);
            sheet.mergeCells(0, 7, 6, 7);

            sheet.addCell(org_name);
            sheet.addCell(org_email);
            sheet.addCell(org_contact);
            sheet.addCell(org_order);
            sheet.addCell(org_client);
            sheet.addCell(org_date);
            sheet.addCell(org_client_email);
            sheet.addCell(org_client_phone);
            sheet.addCell(org_note);

            WritableCellFormat f_table_all = new WritableCellFormat();
            f_table_all.setAlignment(Alignment.CENTRE);
            f_table_all.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);

            Label t_num = new Label(0, 8, "№", f_table_all);
            Label t_article = new Label(1, 8, "Название", f_table_all);
            Label t_count = new Label(2, 8, "Количество шт.", f_table_all);
            Label t_weight = new Label(3, 8, "Вес", f_table_all);
            Label t_gross_weight = new Label(4, 8, "Общий вес", f_table_all);
            Label t_price = new Label(5, 8, "Цена", f_table_all);
            Label t_gross_price = new Label(6, 8, "Сумма", f_table_all);

            sheet.addCell(t_num);
            sheet.addCell(t_article);
            sheet.addCell(t_count);
            sheet.addCell(t_weight);
            sheet.addCell(t_gross_weight);
            sheet.addCell(t_price);
            sheet.addCell(t_gross_price);
            int i;
            int all_count = 0;
            Double all_weight = 0.0;
            Double all_price = 0.0;
            
            for(i = 0; i < basket.getCount(); i++)
            {
                all_count += basket.getPosition(i).getCount();
                all_weight += basket.getPosition(i).getCount() * basket.getPosition(i).getSize().getWeight();
                all_price += basket.getPosition(i).getPrice();

                Label v_num = new Label(0, 9 + i, Integer.toString(i + 1), f_table_all);
                Label v_article = new Label(1, 9 + i, basket.getPosition(i).getProductName(), f_table_all);
                Number v_count = new Number(2, 9 + i, basket.getPosition(i).getCount(), f_table_all);
                Number v_weight = new Number(3, 9 + i, basket.getPosition(i).getSize().getWeight(), f_table_all);
                Formula v_gross_weight = new Formula(4, 9 + i, "C"+(10 + i)+"*D"+(10 + i), f_table_all);
                Number v_price = new Number(5, 9 + i, basket.getPosition(i).getPrice(), f_table_all);
                Formula v_gross_price = new Formula(6, 9 + i, "C"+(10 + i)+"*F"+(10 + i), f_table_all);

                sheet.addCell(v_num);
                sheet.addCell(v_article);
                sheet.addCell(v_count);
                sheet.addCell(v_weight);
                sheet.addCell(v_gross_weight);
                sheet.addCell(v_price);
                sheet.addCell(v_gross_price);
            }

            Label itog = new Label(0, 9 + i, "Итого: ");
            Number g_count = new Number(2, 9 + i, all_count);
            Number g_weight = new Number(4, 9 + i,all_weight);
            Number g_price = new Number(6, 9 + i, all_price);

            sheet.addCell(itog);
            sheet.addCell(g_count);
            sheet.addCell(g_weight);
            sheet.addCell(g_price);

            workbook.write();
            workbook.close();
        }
        catch (Exception e)
        {

        }
    }

}