package com.auruscatalog.catalog.utility;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;

import com.auruscatalog.catalog.R;

import java.io.Serializable;
import java.util.Stack;
import java.util.StringTokenizer;

/**
 * Created by sparrow on 24.11.15.
 */
public class FragmentManagerApp implements Serializable
{
    public static String MAIN = "main";

    public static String CARD = "card";

    public static String CART = "cart";

    public static String CATALOG = "catalog";

    public static String NEWS = "news";

    public static String ABOUT = "about";

    public static String FILTER = "filter";

    public static String ORDER = "order";

    public static String IMAGEVIEW = "imageview";

    public static String EXPORT = "exportdialog";

    public static String START = "start";


    private FragmentTransaction fragmentTransaction;

    private static Stack<String> mFragmentTagStack;

    private static FragmentManager mFragmentManager;

    public FragmentManagerApp(FragmentManager manager)
    {
        mFragmentManager = manager;
    }

    /**
     * Добавляет элемент на вершину стека Fragment'ов
     * @param fragment Fragment для добавления
     * @param tag Tag Fragment'а для добавления
     */
    public void addFragment(Fragment fragment, String tag)
    {
        if(mFragmentTagStack == null)
            mFragmentTagStack = new Stack<>();

        mFragmentTagStack.push(tag);
        fragmentTransaction = mFragmentManager.beginTransaction();

        fragmentTransaction
                .add(R.id.fragment_main, fragment, tag)
                .commit();
    }

    /**
     * Удаляет верхний Fragment из стека фрагментов
     */
    public void popFragment()
    {
        if(mFragmentTagStack != null && !mFragmentTagStack.empty() && mFragmentTagStack.peek() != null)
        {
            Fragment fragment = mFragmentManager.findFragmentByTag(mFragmentTagStack.pop());

            fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction
                    .remove(fragment)
                    .commit();

        }
    }

    /**
     * Проверяет является ли верхний элемент стека Fragment'ов указанным по тегу
     * @param tag Тег для проверки
     * @return Возвращает true если такой Fragment лежит на вершние стека, иначе false
     */
    public boolean isTopFromTag(String tag)
    {
        if(mFragmentTagStack != null && !mFragmentTagStack.empty())
        {
            if(tag == null || tag.equals(""))
                return false;

            if(mFragmentTagStack.peek().equals(tag))
            {
               return mFragmentManager.findFragmentByTag(tag) != null;
            }
            else
                return false;
        }
        else
            return false;
    }

    /**
     * Возвращает верхний элемент стека Fragment'ов
     * @return
     */
    public Fragment getTop()
    {
        return mFragmentTagStack != null && !mFragmentTagStack.empty() ? mFragmentManager.findFragmentByTag(mFragmentTagStack.peek()) : null;
    }

    /**
     * Проверяет не пуст ли стек Fragment'ов
     * @return true если не пуст, иначе false
     */
    public boolean isNotVoid()
    {
        return mFragmentTagStack != null && !mFragmentTagStack.empty();
    }

    /**
     * Возвращает кол-во элементов в стеке Fragment'ов
     * @return
     */
    public int getCountStack()
    {
        return mFragmentTagStack != null ? mFragmentTagStack.size() : 0;
    }
}
